#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>



using namespace spotify::common;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_authorizer(nullptr)
    , m_player(nullptr)
    , m_client(nullptr)
    , m_add_playlist(nullptr)
    , m_last_id("")
{
    ui->setupUi(this);

    // showing authorizing frame, hiding working frame
    ui->fraWork->setVisible(false);
    ui->fraAuthorize->setVisible(true);

    // browser for authorization
    m_authorizer = new QWebEngineView(this);
    ui->lyoAuthorize->addWidget(m_authorizer);

    // browser for playing
    m_player = new QWebEngineView(/*this*/);
    ui->lyoPlayer->addWidget(m_player);

    // configuring the table of searched tracks
    ui->tabTracks->setRowCount(0);
    ui->tabTracks->setColumnCount(3);
    ui->tabTracks->setColumnHidden(ID_COL,true);
    //ui->tabTracks->setColumnWidth(NAME_COL,ui->tabTracks->width());
    ui->tabTracks->setColumnHidden(URL_COL,true);
    ui->btnCleanTracks->setVisible(false);

    // configuring the tree of playlists
    ui->trePlaylists->setColumnCount(3);
    ui->trePlaylists->setColumnHidden(ID_COL,true);
    ui->trePlaylists->setColumnHidden(URL_COL,true);

    // add new playlist
    m_add_playlist = new add_playlist(this);
    m_add_playlist->setVisible(false);

    QObject::connect(m_add_playlist, SIGNAL(ok(QString))
                     , this, SLOT(on_ok_add_playlist(QString)));

    QObject::connect(m_add_playlist, SIGNAL(cancel())
                     , this, SLOT(on_cancel_add_playlist()));


     //configuring Spotify client

    m_client = new bus::client("6c3058ffc39c45ae98f8426ca85d8078"
                                   , "c212c2515bed4ae199c485dd937f1832"
                                   , m_authorizer
                                   , this);

    QObject::connect(m_client, SIGNAL(authorized())
                     , this, SLOT(on_authorized()));

    QObject::connect(m_client, SIGNAL(problem(ent::msg_id))
                     , this, SLOT(on_problem(ent::msg_id)));

    QObject::connect(this, SIGNAL(search_track(QString))
                     , m_client, SLOT(on_search_track(QString)));

    QObject::connect(this, SIGNAL(clean_searched_tracks())
                     , m_client, SLOT(on_clean_searched_tracks()));

    QObject::connect(m_client, SIGNAL(track_searched_found())
                     , this, SLOT(on_track_searched_found()));
    QObject::connect(m_client, SIGNAL(track_searched_not_found())
                     , this, SLOT(on_track_searched_not_found()));

    QObject::connect(this, SIGNAL(read_user_public())
                     , m_client, SLOT(on_read_user_public()));

    QObject::connect(m_client, SIGNAL(user_public_read())
                     , this, SLOT(on_user_public_read()));

    QObject::connect(m_client, SIGNAL(user_playlists_loaded()),
                     this, SLOT(on_user_playlists_loaded()));

    QObject::connect(this
                     , SIGNAL(create_playlist(ent::entity::id,QString))
                     , m_client
                     , SLOT(on_create_playlist(ent::entity::id,QString)));
    QObject::connect(m_client, SIGNAL(playlist_created())
                     , this, SLOT(on_playlist_created()));

    QObject::connect(this, SIGNAL(insert_track(ent::track,ent::entity::id))
                     , m_client
                     , SLOT(on_insert_track(ent::track,ent::entity::id)));

    QObject::connect(m_client, SIGNAL(track_inserted())
                     , this, SLOT(on_track_inserted()));

    QObject::connect(this
                     , SIGNAL(remove_track(ent::track::id,ent::playlist::id))

                     , m_client
                     , SLOT(on_remove_track(ent::track::id,ent::playlist::id)));

    QObject::connect(m_client, SIGNAL(track_removed())
                     , this, SLOT(on_track_removed()));

    QObject::connect(this, SIGNAL(remove_playlist(ent::playlist::id))
                     , m_client, SLOT(on_remove_playlist(ent::playlist::id)));
    QObject::connect(m_client, SIGNAL(playlist_removed())
                     , this, SLOT(on_playlist_removed()));


    ui->fraAuthorize->setVisible(true);

    // authorizing access to Spotify
    m_client->authorize<bus::read_user_public_action>();


    this->showMaximized();
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_authorized()
{
    ui->fraWork->setVisible(true);
    ui->fraAuthorize->setVisible(false);

    emit read_user_public();

}

void MainWindow::on_user_public_read() {
    QString _user = m_client->get_user()->get_name();
    if (_user.size() == 0) {
        _user = m_client->get_user()->get_id();
    }
    ui->lblPlaylist->setText(tr("Playlist de ") + _user);
}

void MainWindow::on_track_searched_found() {
    qDebug() << "MainWindow::on_track_searched";


    ui->tabTracks->setColumnWidth(1,ui->tabTracks->width());

    using namespace spotify::common::ent;

    const tracks & _tracks = m_client->get_searched_tracks();
    fill_table(ui->tabTracks, _tracks.begin(), _tracks.end());
    ui->btnCleanTracks->setVisible(true);
}

void MainWindow::on_user_playlists_loaded() {
    const ent::playlists& _playlists= m_client->get_playlists();

    ent::playlists::const_iterator _end_p = _playlists.end();

    for (ent::playlists::const_iterator _ite_play = _playlists.begin();
         _ite_play != _end_p;
         ++_ite_play) {
        QTreeWidgetItem * _item_play = new QTreeWidgetItem(ui->trePlaylists);
        _item_play->setText(ID_COL, _ite_play->get_id());
        _item_play->setText(NAME_COL, _ite_play->get_name());
        _item_play->setText(URL_COL, _ite_play->get_url().toString());
        _item_play->setText(TYPE_COL, TYPE_COL_PLAY );

        const ent::tracks & _tracks = _ite_play->get_tracks();
        ent::tracks::const_iterator _end_t = _tracks.end();
        for (ent::tracks::const_iterator _ite_track = _tracks.begin();
             _ite_track != _end_t;
             ++_ite_track) {
            QTreeWidgetItem * _item_track = new QTreeWidgetItem();
            _item_track->setText(ID_COL, _ite_track->get_id());
            _item_track->setText(NAME_COL, _ite_track->get_name());
            _item_track->setText(URL_COL, _ite_track->get_url().toString());
            _item_track->setText(TYPE_COL, TYPE_COL_TRACK);
            _item_play->addChild(_item_track);
        }
    }


}

void MainWindow::on_playlist_created()
{
    refresh_playlists();
}

void MainWindow::on_track_inserted()
{
    refresh_playlists();
}

void MainWindow::on_track_removed()
{
    refresh_playlists();
}

void MainWindow::on_playlist_removed()
{
    ui->trePlaylists->clear();
    on_user_playlists_loaded();
}

void MainWindow::on_track_searched_not_found()
{
    QMessageBox::information(this
                             , tr("INFO")
                             , tr("Nenhuma música foi encontrada"));
}

void MainWindow::on_problem(spotify::common::ent::msg_id p_msg_id) {
    QMessageBox::critical(this
                          , tr("ERRO")
                          , m_client->get_message(p_msg_id));
}

void MainWindow::on_btnSearch_clicked()
{
    emit search_track(ui->txtSearch->text());
}


void MainWindow::on_btnCleanTracks_clicked()
{
    ui->tabTracks->setRowCount(0);
    ui->btnCleanTracks->setVisible(false);

    emit clean_searched_tracks();
}

void MainWindow::on_btnAddPlaylist_clicked()
{
    m_add_playlist->show();
}

void MainWindow::on_ok_add_playlist(const QString & p_playlist_name) {

    m_add_playlist->setVisible(false);
    emit create_playlist(m_client->get_user()->get_id()
                         , p_playlist_name);
}

void MainWindow::on_cancel_add_playlist() {
    m_add_playlist->setVisible(false);
}



void MainWindow::on_btnToLeft_clicked()
{
    int _row = ui->tabTracks->currentRow();

    if (_row == -1) {
        QMessageBox::information(this
                                 , tr("INFO")
                                 , tr("Nenhuma música selecionada"));
        return;
    }

    QList<QTreeWidgetItem *> _itens = ui->trePlaylists->selectedItems();
    if (_itens.empty()) {
        QMessageBox::information(this
                                 , tr("INFO")
                                 , tr("Nenhuma playlist selecionada"));
        return;
    }

    // selected track data
    ent::entity::id _track_id = ui->tabTracks->item(_row, ID_COL)->text();
    QString _name = ui->tabTracks->item(_row, NAME_COL)->text();
    QUrl _url = ui->tabTracks->item(_row, URL_COL)->text();

    // selecting playlist
    QTreeWidgetItem * _item = _itens.at(0);

    if (_item->text(TYPE_COL) == TYPE_COL_TRACK) {
        _item = _item->parent();
    }

    ent::entity::id _playlist_id = _item->text(ID_COL);


    emit  insert_track(ent::track(_track_id, _url, _name)
                       , _playlist_id);
}



void MainWindow::on_btnPlaySearchedTrack_clicked()
{
    int _row = ui->tabTracks->currentRow();
    if ( (_row >=0) && (_row < ui->tabTracks->rowCount()) ) {
        QUrl _url (ui->tabTracks->item(_row, URL_COL)->text());
        qDebug() << "MainWindow::on_btnPlaySearchedTrack_clicked, url = "
                 << _url;
        m_player->load(_url);
    }
}

void MainWindow::on_btnPlayPlaylistTrack_clicked()
{
    QList<QTreeWidgetItem *> _itens = ui->trePlaylists->selectedItems();
    if (_itens.empty()) {
        return;
    }

    QTreeWidgetItem * _item = _itens.at(0);

    if (_item->text(TYPE_COL) == TYPE_COL_TRACK) {
        QUrl _url (_item->text(URL_COL));
        qDebug() << "MainWindow::on_btnPlayPlaylistTrack_clicked, url = "
                 << _url;

        m_player->load(_url);
    }

}

void MainWindow::on_btnDelPlaylistTrack_clicked()
{
    QList<QTreeWidgetItem *> _itens = ui->trePlaylists->selectedItems();
    if (_itens.empty()) {
        QMessageBox::information(this, tr("INFO")
                                 , tr("Selecione uma música ou playlist para "
                                      "excluir "));
        return;
    }

    QTreeWidgetItem * _item = _itens.at(0);

    if (_item->text(TYPE_COL) == TYPE_COL_TRACK) {
        emit remove_track(_item->text(ID_COL), _item->parent()->text(ID_COL));
    }
    else if (_item->text(TYPE_COL) == TYPE_COL_PLAY) {
        emit remove_playlist(_item->text(ID_COL));
    }
}

//template <typename t_const_iterator>
void MainWindow::fill_table(QTableWidget * p_table
                , ent::tracks::const_iterator p_begin
                , ent::tracks::const_iterator p_end) {

    for (ent::tracks::const_iterator _ite = p_begin; _ite != p_end; ++_ite) {
        p_table->insertRow( p_table->rowCount() );
        int _last_row = p_table->rowCount() - 1;
        p_table->setItem(_last_row
                         , ID_COL
                         , new QTableWidgetItem(_ite->get_id()));

        p_table->setItem(_last_row
                         , NAME_COL
                         , new QTableWidgetItem(_ite->get_name()));

        p_table->setItem(_last_row
                         , URL_COL
                         , new QTableWidgetItem(_ite->get_url().toString()));
    }
    p_table->setColumnWidth(NAME_COL,p_table->width());
}

void MainWindow::refresh_playlists()
{
    ui->trePlaylists->clear();
    on_user_playlists_loaded();

    if (m_last_id == "") {
        return;
    }

    QTreeWidgetItemIterator _ite(ui->trePlaylists);
    while (*_ite) {
        qDebug() << "MainWindow::refresh_playlists, id = "
                 << (*_ite)->text(ID_COL);
        if ( ((*_ite)->text(TYPE_COL)) == TYPE_COL_PLAY) {
            if ((*_ite)->text(ID_COL) == m_last_id) {
                (*_ite)->setSelected(true);
                (*_ite)->setExpanded(true);
                break;
            }
            if ((*_ite)->text(ID_COL) > m_last_id) {
                QTreeWidgetItemIterator _pre(_ite);
                --_pre;
                (*_pre)->setSelected(true);
                (*_pre)->setExpanded(true);
                break;
            }
        }
        ++_ite;
    }

}

void MainWindow::on_trePlaylists_itemClicked(QTreeWidgetItem *item, int /*column*/)
{
    qDebug() << "MainWindow::on_trePlaylists_itemClicked, last id antes = "
             << m_last_id;
    QTreeWidgetItem *_actual = item;
    if (item->text(TYPE_COL) == TYPE_COL_TRACK) {
        _actual = _actual->parent();
    }
    m_last_id = _actual->text(ID_COL);
    qDebug() << "MainWindow::on_trePlaylists_itemClicked, last id depois = "
             << m_last_id;

}
