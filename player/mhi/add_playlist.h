#ifndef ADD_PLAYLIST_H
#define ADD_PLAYLIST_H

#include <QDialog>

namespace Ui {
class add_playlist;
}

class add_playlist : public QDialog
{
    Q_OBJECT

public:
    explicit add_playlist(QWidget *parent = 0);
    ~add_playlist();

signals:
    void ok(const QString &);
    void cancel();

private slots:
    void on_btnOk_clicked();

    void on_btnCancel_clicked();

private:
    Ui::add_playlist *ui;
};

#endif // ADD_PLAYLIST_H
