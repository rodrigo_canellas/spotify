#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>
#include <QTreeWidget>
#include <QWebEngineView>
#include <spotify/common/bus/actions.h>
#include <spotify/common/bus/client.h>
#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/ent/entity.h>

#include <spotify/player/mhi/add_playlist.h>

#define NAME_COL 0
#define ID_COL 1
#define URL_COL 2
#define TYPE_COL 3

#define TYPE_COL_PLAY "P"
#define TYPE_COL_TRACK "T"



using namespace spotify::common;

namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:



    // slots to receive response of actions

    void on_problem(ent::msg_id);

    void on_authorized();

    void on_user_public_read();

    void on_track_searched_found();

    void on_track_searched_not_found();

    void on_user_playlists_loaded();

    void on_playlist_created();

    void on_track_inserted();

    void on_track_removed();

    void on_playlist_removed();



    // UI slots
    void on_btnSearch_clicked();

    void on_btnCleanTracks_clicked();

    void on_btnAddPlaylist_clicked();

    void on_ok_add_playlist(const QString &);

    void on_cancel_add_playlist();

    void on_btnToLeft_clicked();

    void on_btnPlaySearchedTrack_clicked();

    void on_btnPlayPlaylistTrack_clicked();

    void on_btnDelPlaylistTrack_clicked();

    void on_trePlaylists_itemClicked(QTreeWidgetItem *item, int column);

signals:


    // signals to execute actions

    void read_user_public();

    void search_track(const QString & p_criteria);

    void clean_searched_tracks();

    void create_playlist(const ent::entity::id & p_user_id
                         , const QString & p_playlist_name);

    void insert_track(const ent::track &p_track
                      , const ent::entity::id & p_playist);

    void remove_track(const ent::track::id &p_track_track
                      , const ent::playlist::id & p_playist_id);

    void remove_playlist(const ent::playlist::id & p_playist_id);

private:
    void fill_table(QTableWidget *p_table
                    , ent::tracks::const_iterator p_begin
                    , ent::tracks::const_iterator p_end);

    void refresh_playlists();

    Ui::MainWindow *ui;
    QWebEngineView * m_authorizer;
    QWebEngineView * m_player;
    bus::client * m_client;
    add_playlist * m_add_playlist;
    ent::entity::id m_last_id;

};

#endif // MAINWINDOW_H
