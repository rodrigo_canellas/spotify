#include "add_playlist.h"
#include "ui_add_playlist.h"

add_playlist::add_playlist(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::add_playlist)
{
    ui->setupUi(this);
}

add_playlist::~add_playlist()
{
    delete ui;
}

void add_playlist::on_btnOk_clicked()
{
    if (ui->txtPlaylistName->text().size() == 0) {
        emit cancel();
    }
    else {
        emit ok(ui->txtPlaylistName->text());
    }
}

void add_playlist::on_btnCancel_clicked()
{
    emit cancel();
}
