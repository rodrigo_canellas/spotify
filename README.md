# spotify

## About
This repository contains a library that allows essencial access to Spotify services; and a application, a very simple music player and playlist creator.
This document describes how to build the library and the application.

## Design 
If, however, you want to know more about how library and program were designed, please read the [Software Structure](https://github.com/Canellas/spotify/blob/master/doc/design/software_structure.pdf) document.

## Dependencies

### External
Both library and application depends on Qt version 5.11, and QtCreator 4.6.1

### Internal
You must also clone repository https://github.com/Canellas/configuration, as all the projects use build configurations defined in this repository

### Environment variable

After you clone this repository and https://github.com/Canellas/configuration, it is necessary to define a environment variable called **DVP**, which must contain the path to the parent directory where the repositories were cloned. 
For example, in a Linux box, if you clone the repositories to **/opt/dvp**, that must be the value of **DVP**. 

## Building the library

### Directory structure

After you clone this repository, you will see these directories under **$DVP**:

	├── configuration
	├── spotify
	
In **$DVP/spotify** there are these directories:

	├── common
	├── doc
	└── player

In **$DVP/spotify/common** we have:

	├── _builder
	├── bus
	├── doc
	├── ent
	├── per
	├── res
	└── tst

By now, we are interested in the **_builder** directory. In this directory there can be many different build systems for the library, each one in a separate directory. The one used was based on **qmake**, used from **QtCreator**, so in **$DVP/spotify/common/_builder** we have a single directory:
	
	└── qmake

And in this directory we have the **spotify.common.pro** file, which contains the necessary definitions to generate the library. 

### The QtCreator project 

Start QtCreator, and open the **$DVP/spotify/common/_builder/qmake/spotify.common.pro** file.

It is better if you turn **Shadow build** off. Click on the ![](https://github.com/Canellas/spotify/blob/master/doc/qtcreator_project.png) icon, and the in **Build Settings**, **General** section, leave **Shadow Build** unmarked:

 ![](https://github.com/Canellas/spotify/blob/master/doc/qtcreator_shadowbuild_unmarked.png) 
 
**_Remember_** to choose a kit with **Qt 5.11**.

Now you can choose **Build/Build Project "spotify.common"** from QtCreator menu to build the library.

## Destination Directory

As already said, the https://github.com/Canellas/configuration repository contains files that are used to help configure other projects. 

One of these configurations define the directories to where the products generated will go.

Below **$DVP** is the **products** directory, and under it a directory tree that reflects the settings used in the project.

For example, if you build using **Debug** and **linux-g++** in QtCreator, as the image shows:

![](https://github.com/Canellas/spotify/blob/master/doc/qtcreator_build_settings.png) 

The **$DVP/products** will contain:

	├── linux-g++
	│   └── debug
	│       └── lib
	│           ├── libspotify.common.a
	│           ├── libspotify.common.so -> libspotify.common.so.0.0.2
	│           ├── libspotify.common.so.0 -> libspotify.common.so.0.0.2
	│           ├── libspotify.common.so.0.0 -> libspotify.common.so.0.0.2
	│           └── libspotify.common.so.0.0.2


## Building the player

### Directory Structure

The player has a directory structure similar to the lib. In **$DVP/spotify/player** we have:


	├── _builder
	└── mhi
	
And, just like we saw in **$DVP/spotify/common/_builder**, in **$DVP/spotify/player/_builder** there is:

	
	└── qmake
	    ├── player.qrc
	    ├── spotify.player.pro

The **spotify.player.pro** contains the definitions necessary to generate the application, and the **player.qrc** contains files used as resources in the application.

### The QtCreator project 
Start QtCreator, and open the **$DVP/spotify/common/_builder/qmake/spotify.player.pro** file.

It is better if you turn **Shadow build** off. Click on the ![](https://github.com/Canellas/spotify/blob/master/doc/qtcreator_project.png) icon, and the in **Build Settings**, **General** section, leave **Shadow Build** unmarked:

 ![](https://github.com/Canellas/spotify/blob/master/doc/qtcreator_shadowbuild_unmarked.png) 
 
**_Remember_** to choose a kit with **Qt 5.11**.

Now you can choose **Build/Build Project "spotify.player"** from QtCrator menu to build the library.

**_Important_** The **spotify.player** links to the static version of **spotify.common** lib, and thanks to the files in  https://github.com/Canellas/configuration, the application project knows where the lib is located.

## Destination Directory

Once again, the https://github.com/Canellas/configuration repository contains files that are used to help configure other projects. 

One of these configurations define the directories to where the products generated will be.

Below **$DVP** is the **products** directory, and under it a directory tree that reflects the settings used in the project.

For example, if you build using **Debug** and **linux-g++** in QtCreator, as the image shows:

![](https://github.com/Canellas/spotify/blob/master/doc/qtcreator_build_settings.png) 


After building the application, **$DVP/products** will contain:

	├── linux-g++
	│   └── debug
	│       ├── bin
	│       │   └── spotify.player
	│       └── lib
	│           ├── libspotify.common.a
	│           ├── libspotify.common.so -> libspotify.common.so.0.0.2
	│           ├── libspotify.common.so.0 -> libspotify.common.so.0.0.2
	│           ├── libspotify.common.so.0.0 -> libspotify.common.so.0.0.2
	│           └── libspotify.common.so.0.0.2

