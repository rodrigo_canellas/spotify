#include <spotify/common/bus/permissions.h>

using namespace spotify::common::bus;

// ----------------------------------------------------------------------------
const permissions permissions::user_read_public            (0x0000000);
const permissions permissions::user_top_read               (0x0000001);
const permissions permissions::user_read_recently_played   (0x0000002);
const permissions permissions::user_library_modify         (0x0000004);
const permissions permissions::user_library_read           (0x0000008);
const permissions permissions::playlist_modify_public      (0x0000010);
const permissions permissions::playlist_modify_private     (0x0000040);
const permissions permissions::playlist_read_collaborative (0x0000080);
const permissions permissions::playlist_read_private       (0x0000200);
const permissions permissions::user_read_private           (0x0000400);
const permissions permissions::user_read_email             (0x0000800);
const permissions permissions::user_read_birthdate         (0x0001000);
const permissions permissions::user_follow_modify          (0x0002000);
const permissions permissions::user_follow_read            (0x0004000);
const permissions permissions::user_read_currently_playing (0x0008000);
const permissions permissions::user_read_playback_state    (0x0010000);
const permissions permissions::user_modify_playback_state  (0x0020000);
const permissions permissions::streaming                   (0x0040000);

// ----------------------------------------------------------------------------
permissions::permissions(value p_value)
    : m_value(p_value){}


// ----------------------------------------------------------------------------
permissions::permissions(permissions && p_permissions)
    : m_value(std::move(p_permissions.m_value)) {}

// ----------------------------------------------------------------------------
permissions::permissions(const permissions & p_permissions)
    : m_value(p_permissions.m_value) {}


// ----------------------------------------------------------------------------
QString permissions::to_string() const {
    QString _str;

    if (m_value & user_top_read.m_value) {
        _str += "user-top-read";
    }

    if (m_value & user_read_recently_played.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "user-read-recently-played";
    }

    if (m_value & user_library_modify.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "user-library-modify";
    }

    if (m_value & user_library_read.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "user-library-read";
    }

    if (m_value & playlist_modify_public.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "playlist-modify-public";
    }

    if (m_value & playlist_modify_private.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "playlist-modify-private";
    }

    if (m_value & playlist_read_collaborative.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "playlist-read-collaborative";
    }

    if (m_value & playlist_read_private.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "playlist-read-private";
    }

    if (m_value & user_read_private.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "user-read-private";
    }

    if (m_value & user_read_email.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "user-read-email";
    }

    if (m_value & user_read_birthdate.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "user-read-birthdate";
    }

    if (m_value & user_follow_modify.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "user-follow-modify";
    }

    if (m_value & user_follow_read.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "user-follow-read";
    }

    if (m_value & user_read_currently_playing.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "user-read-currently-playing";
    }

    if (m_value & user_read_playback_state.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "user-read-playback-state";
    }

    if (m_value & user_modify_playback_state.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "user-modify-playback-state";
    }

    if (m_value & streaming.m_value) {
        if (_str.size() > 0) {
            _str += " ";
        }
        _str += "streaming";
    }

    return _str;
}

//// ----------------------------------------------------------------------------
//void permissions::use_all() {
//    m_value = user_top_read.m_value | user_read_recently_played.m_value | user_library_modify.m_value |
//            user_library_read.m_value | playlist_modify_public.m_value |
//            playlist_modify_private.m_value | playlist_read_collaborative.m_value |
//            playlist_read_private.m_value | user_read_private.m_value | user_read_email.m_value |
//            user_read_birthdate.m_value | user_follow_modify.m_value | user_follow_read.m_value |
//            user_read_currently_playing.m_value | user_read_playback_state.m_value |
//            streaming;
//}

// ----------------------------------------------------------------------------
permissions & permissions::operator+=(const permissions & p_permissions) {
    if (this != &p_permissions) {
        m_value = p_permissions.m_value;
    }
    return *this;
}

// ----------------------------------------------------------------------------
permissions & permissions::operator+=(permissions && p_permissions) {
    if (this != &p_permissions) {
        m_value = std::move(p_permissions.m_value);
    }
    return *this;
}
