#include <spotify/common/bus/client.h>
#include <algorithm>


#include <QDebug>

using namespace spotify::common::bus;

// -----------------------------------------------------------------------------
client::client(const QString &p_client_id
                 , const QString &p_client_secret
                 , QWebEngineView *p_authorizer
                 , bool p_close_browser
//                 , const permissions & p_permissions
                 , QObject *p_parent
                 , qint16 p_port)
    : QObject(p_parent)
    , m_authorizer_browser(p_authorizer)
    , m_close_browser_after_authorized(p_close_browser)
    , m_is_authorized(false)
    , m_db(this, QCoreApplication::applicationDirPath()){

    if (!m_authorizer_browser) {
        throw std::runtime_error("Browser usado para autorização é nulo");
    }

    auto replyHandler = new QOAuthHttpServerReplyHandler(p_port, this);

    m_spotify.setReplyHandler(replyHandler);
    m_spotify.setAuthorizationUrl(QUrl("https://accounts.spotify.com/authorize"));
    m_spotify.setAccessTokenUrl(QUrl("https://accounts.spotify.com/api/token"));
    m_spotify.setClientIdentifier(p_client_id);
    m_spotify.setClientIdentifierSharedKey(p_client_secret);

    connect(&m_spotify, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser,
            this, &client::authorizeWithBrowser);

    connect(&m_spotify, &QOAuth2AuthorizationCodeFlow::statusChanged,
            this, &client::authStatusChanged);

    connect(&m_spotify, &QOAuth2AuthorizationCodeFlow::granted,
            this, &client::granted);
}

// -----------------------------------------------------------------------------
QString client::get_message(spotify::common::ent::msg_id p_msg_id) {
    if (m_messages.empty()) {
        if (!m_db.load_messages(m_messages)) {
            return "";
        }
    }
    return m_messages.at(p_msg_id);
}

// -----------------------------------------------------------------------------
void client::authorizeWithBrowser(const QUrl &p_url)
{
    m_authorizer_browser->load(p_url);
    m_authorizer_browser->show();
}

// -----------------------------------------------------------------------------
void client::authStatusChanged(QAbstractOAuth::Status p_status)
{
    if (p_status == QAbstractOAuth::Status::NotAuthenticated) {
        emit problem(ent::error_authorizing_user);
    }
}

// -----------------------------------------------------------------------------
void client::granted()
{
    if (m_close_browser_after_authorized) {
        m_authorizer_browser->setVisible(false);
        m_authorizer_browser->close();
    }
    m_is_authorized = true;
    emit authorized();
}

// -----------------------------------------------------------------------------
QJsonDocument client::request(const QUrl & p_url) {

    qDebug() << "client::send started, url = " << p_url.toString();

    QEventLoop _loop;

    QNetworkReply * _reply = m_spotify.get(p_url);

    connect(_reply, SIGNAL(finished()), &_loop, SLOT(quit()));

    _loop.exec();

    const QByteArray _data = _reply->readAll();

    QJsonDocument _json = QJsonDocument::fromJson(_data);

    //qDebug() << "client::send, JSon = " << _json;

    _reply->deleteLater();

    return _json;
}

// -----------------------------------------------------------------------------
void client::authorize() {
    if (m_is_authorized) {
        qDebug() << "client::authorize - already authorized";
        return;
    }

    m_spotify.setScope(permissions::user_read_public.to_string());

    m_spotify.grant();

}

// -----------------------------------------------------------------------------
void client::on_read_user_public() {
    qDebug() << "client::on_read_user_public() started";

    ent::user _user;

    if (!m_db.get_user_public(_user)) {
        emit problem(ent::error_loading_user_public);
        return;
    }

    qDebug() << "client::on_read_user_public(): "
             << "name = '" << _user.get_name() <<  "' "
             << "id = '" + _user.get_id() << "'";

    if (!m_user) {
        m_user = std::shared_ptr<ent::user>(new ent::user(std::move(_user)));
    }

    emit user_public_read();

    if (!m_db.load_playlists(m_user->get_id(), m_playlists) ) {
        emit problem(ent::error_loading_playlists);
    }

    qDebug() << "client::on_read_user_public(): "
             << "name = '" << m_user->get_name() <<  "' "
             << "id = '" + m_user->get_id() << "'";

    emit user_playlists_loaded();
}


// -----------------------------------------------------------------------------
void client::on_search_track(const QString & p_criteria) {

    qDebug() << "client::on_search_track started";

    QString _formated_criteria (query_formater()(p_criteria));

    ent::tracks _tracks;

    if (!m_db.search_track(_formated_criteria, _tracks)) {
        emit problem(ent::error_searching_for_tracks);
    }


    if (!_tracks.size()) {
        qDebug() << "Nenhum trilha encontrada";
        emit track_searched_not_found();
        return;
    }

    spotify::common::ent::merge(m_searched_tracks, _tracks);

    emit track_searched_found();
}

// -----------------------------------------------------------------------------
void client::on_create_playlist(const ent::entity::id & p_user_id
                                , const QString & p_name) {
    qDebug() << "client::on_create_local_playlist started";

    ent::playlist _playlist;

    if (!m_db.insert_playlist(p_user_id, p_name, _playlist)) {
        emit problem(ent::error_creating_playlist);
    }

    m_playlists.insert(_playlist);

    emit playlist_created();
}

// -----------------------------------------------------------------------------
void client::on_clean_searched_tracks() {
    m_searched_tracks.clear();
}

// -----------------------------------------------------------------------------
void client::on_insert_track(const ent::track &p_track
                             , const ent::entity::id &p_playist_id)
{

    if (!m_db.insert_track(p_track, p_playist_id)) {
        emit problem(ent::error_inserting_track_into_playlist);
    }
    else {

//    ent::playlists::iterator _ite =
//            std::find_if(m_playlists.begin()
//                         , m_playlists.end()
//                         , [p_playist_id](const ent::playlist & p_playist)
//    {
//        return p_playist.get_id() == p_playist_id;
//    });

//    if (_ite == m_playlists.end()) {
//        qDebug() << "client::on_insert_track, _ite == m_playlists.end()";
//        m_playlists.clear();
//        m_db.load_playlists(m_user->get_id(), m_playlists);
//    }
//    else {
//        qDebug() << "client::on_insert_track, _ite != m_playlists.end()";
//        ent::playlist * _playlist = &(*_ite);
//        _playlist->add(p_track);
//    }

        m_playlists.clear();
        m_db.load_playlists(m_user->get_id(), m_playlists);

        emit track_inserted();
    }
}

// -----------------------------------------------------------------------------
void client::on_remove_track(const ent::entity::id &p_track_id
                             , const ent::entity::id &p_playlist_id) {

    if (m_db.remove_track(p_track_id, p_playlist_id)) {
        m_playlists.clear();
        m_db.load_playlists(m_user->get_id(), m_playlists);
        emit track_removed();
    }
    else {
        emit problem(ent::error_removing_track_from_playlist);
    }

}

// -----------------------------------------------------------------------------
void client::on_remove_playlist(const ent::entity::id &p_playlist_id) {

    if (m_db.remove_playlist(p_playlist_id)) {
        m_playlists.clear();
        m_db.load_playlists(m_user->get_id(), m_playlists);
        emit playlist_removed();
    }
    else {
        emit problem(ent::error_removing_playlist);
    }
}
