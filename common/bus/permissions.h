#ifndef spotify_common_bus_permissions__H
#define spotify_common_bus_permissions__H

/// \file In this file is implemented the spotify::common::bus::permissions
/// class

/// \author Rodrigo Canellas

/// \date may/2018

// ==> Headers C++
#include <cstdint>
#include <map>
#include <utility>
#include <iostream>

// ==> Qt Headers
#include <QString>

// ==> 3rd Headers

// ==> Our Headers

// ==> Namespaces
using namespace std;

// ==> Macro Constants

// ==> Macro Commands

// project
namespace spotify {
// artifact
namespace common {
// layer
namespace bus {

// ==> Typedefs

//typedef uint32_t permissions;

// ==> Pre-Declarations

// ==> Attributes
//static const permissions user_read_public            = 0x0000000;
//static const permissions user_top_read               = 0x0000001;
//static const permissions user_read_recently_played   = 0x0000002;
//static const permissions user_library_modify         = 0x0000004;
//static const permissions user_library_read           = 0x0000008;
//static const permissions playlist_modify_public      = 0x0000010;
//static const permissions playlist_modify_private     = 0x0000040;
//static const permissions playlist_read_collaborative = 0x0000080;
//static const permissions playlist_read_private       = 0x0000200;
//static const permissions user_read_private           = 0x0000400;
//static const permissions user_read_email             = 0x0000800;
//static const permissions user_read_birthdate         = 0x0001000;
//static const permissions user_follow_modify          = 0x0002000;
//static const permissions user_follow_read            = 0x0004000;
//static const permissions user_read_currently_playing = 0x0008000;
//static const permissions user_read_playback_state    = 0x0010000;
//static const permissions user_modify_playback_state  = 0x0020000;
//static const permissions streaming                   = 0x0040000;

// ==> Functions

///
/// \brief to_string assembles the permissionss in a format suitable to be
/// sent to Spotify
/// \return
///
//QString permissions_to_string(permissions p_permissions) ;


// ==> Classes

///
/// \brief The permissions class identifies the possible permissions to be
/// requested to Spotify, and which ones are actually to be used.
/// Associated to a permission there are its \p id, defined at
/// https://beta.developer.spotify.com/documentation/general/guides/scopes, a
/// description, by default in Brazilian Portuguese, and a flag, indicating if
/// it should be used in the scope sent to Spotify.
///
class permissions {

    // ---------------------------------------------
public:

    ///
    /// \brief value type of value of a permission
    ///
    typedef uint32_t value;

    ///
    /// \brief permissions
    ///
    permissions() = delete;

    ///
    /// \brief permissions copy contructor
    /// \param p_permissions
    ///
    permissions(const permissions & p_permissions);

    ///
    /// \brief permissions move constructor
    ///
    permissions(permissions && p_permissions);

    /// Destructor
    inline ~permissions() {}

    ///
    /// \brief use_all defines that all the permissions should be used
    ///
//    void use_all();

    ///
    /// \brief to_string assembles the permissions in a format suitable to be
    /// sent to Spotify
    /// \return
    ///
    QString to_string() const;

    ///
    /// \brief operator += allows to incorportate the permissions of another
    /// permissions object
    /// \param p_permissions
    /// \return
    ///
    permissions & operator+=(const permissions & p_permissions);

    ///
    /// \brief operator += allows to incorportate the permissions of another
    /// permissions object
    /// \param p_permissions
    /// \return
    ///
    permissions & operator+=(permissions && p_permissions);

    ///
    /// \brief operator == equal-to operator
    /// \param p_permissions
    /// \return
    ///
    inline bool operator==(const permissions & p_permissions) const {
        return m_value == p_permissions.m_value;
    }

    ///
    /// \brief operator = copy
    /// \param p_permissions
    /// \return
    ///
    inline permissions & operator=(const permissions & p_permissions) {
        m_value = p_permissions.m_value;
        return *this;
    }

    ///
    /// \brief operator = move
    /// \param p_permissions
    /// \return
    ///
    inline permissions & operator=(permissions && p_permissions) {
        m_value = std::move(p_permissions.m_value);
        return *this;
    }

    /// not allowed
    void *operator new[] (size_t) = delete;

    /// not allowed
    void operator delete[] (void *) = delete;

    /// not allowed
    void operator delete (void * p) = delete;

    /// not allowed
    void *operator new(size_t) = delete;

    ///
    /// \brief values
    ///
    static const permissions user_read_public            ;
    static const permissions user_top_read               ;
    static const permissions user_read_recently_played   ;
    static const permissions user_library_modify         ;
    static const permissions user_library_read           ;
    static const permissions playlist_modify_public      ;
    static const permissions playlist_modify_private     ;
    static const permissions playlist_read_collaborative ;
    static const permissions playlist_read_private       ;
    static const permissions user_read_private           ;
    static const permissions user_read_email             ;
    static const permissions user_read_birthdate         ;
    static const permissions user_follow_modify          ;
    static const permissions user_follow_read            ;
    static const permissions user_read_currently_playing ;
    static const permissions user_read_playback_state    ;
    static const permissions user_modify_playback_state  ;
    static const permissions streaming                   ;


    // ---------------------------------------------
    private:

    ///
    /// \brief permissions defines all the permissions as unset
    /// \param p_id a ORed combination of ids
    ///
    permissions(value p_id);


    ///
    /// \brief m_value OR combination of ids
    ///
    value m_value;

};


// ==> Attributes

// ==> Functions
}
}
}


#endif
