#include <spotify/common/bus/actions.h>

#include <QDebug>
#include <spotify/common/bus/client.h>

using namespace spotify::common::bus;

// #############################################################################
// read_user_public
// #############################################################################

// ----------------------------------------------------------------------------
const permissions
read_user_public_action::
permission = permissions::user_read_public;



// #############################################################################
// search_track_action
// #############################################################################

// ----------------------------------------------------------------------------
const permissions
search_track_action::
permission = permissions::user_read_public;



// #############################################################################
// create_local_playlist_action
// #############################################################################

// ----------------------------------------------------------------------------
const permissions
create_local_playlist_action::
permission = permissions::user_read_public;
