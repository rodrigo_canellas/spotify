#ifndef spotify_common_bus_client__h
#define spotify_common_bus_client__h


/// \file In this file is implemented spotify::common::bus::client,
/// which access services from Spotify
/// (https://beta.developer.spotify.com/documentation/web-api/reference/

/// \author Rodrigo Canellas

/// \date may/2018

// ==> Headers C++
#include <cstdint>
#include <set>

// ==> Qt Headers
#include <QObject>
#include <QtNetworkAuth>
#include <QOAuth2AuthorizationCodeFlow>
#include <QString>
#include <QWebEngineView>
#include <QEventLoop>

// ==> 3rd Headers

// ==> Our Headers
#include <spotify/common/bus/permissions.h>
#include <spotify/common/bus/actions.h>

#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/messages.h>

#include <spotify/common/per/db_local.h>


// ==> Namespaces
using namespace std;

// ==> Macro Constants

// ==> Macro Commands

// manufacturer

// project
namespace spotify {
// artifact
namespace common {
// layer
namespace bus {

// ==> Typedefs

// ==> Pre-Declarations

// ==> Attributes

// ==> Functions

// ==> Classes

////
/// \brief The query_formater struct formatas a query
///
struct query_formater {
    QString operator()(const QString & p_str) {
        QString _res;
        int16_t _size = p_str.size();
        for (int16_t _i =0 ; _i < _size; ++_i) {
            if (p_str[_i] == ' ') {
                _res += "%20";
            }
            else {
                _res += p_str[_i];
            }

        }
        return _res;
    }
};


///
/// \brief The client allows access to Spotify
///
class client : public QObject, public per::i_sender {

    Q_OBJECT

// ---------------------------------------------
public:

    ///
    /// \brief client creates an instance of the \p client class
    /// \param p_client_id is the id of the app in Spotify
    /// (https://beta.developer.spotify.com/dashboard/)
    /// \param p_client_secret is a kind of password of the app in the Spotify
    /// \param p_authorizer is a web browser that will display the login
    /// interface to the user
    /// \param p_close_browser defines if the browser \p p_authorizer should be
    /// closed after the authorization process
    /// \param p_parent is the QObject that owns this instance
    /// \param p_port is the port where the internal web server will listen, in
    /// order to authorize the user
    ///
    client(const QString & p_client_id
            , const QString & p_client_secret
            , QWebEngineView * p_authorizer
            , bool p_close_browser = true
            , QObject *p_parent = nullptr
            , qint16 p_port = 8080);

    /// not allowed
    inline client() = delete;

    /// not allowed
    inline client(const client &) = delete;

    /// not allowed
    inline client(client &&) = delete;

    /// Destructor
    inline ~client() {}

    ///
    /// \brief load_messages loads the messages associated to
    /// spotify::common::ent::msg_id. If this method is not called, a Brazilian
    /// Portuguese message catalog will be loaded the first time a
    /// spotify::common::ent::msg_id is used.
    ///
    /// \param p_language is a code the identifies the language of the messages,
    /// as defined in http://www.lingoes.net/en/translator/langcode.htm. The
    /// default is Brazilian Portuguese
    ///
    void load_messages(const QString & p_language = "pt-br");

    ///
    /// \brief authorize tries to authorize a user, and define the actions that
    /// will be executed
    ///
    /// Each class in t_actions must implemented the attribute
    /// \code
    /// spotify::commmon::bus::permissions permissions
    /// \endcode
    /// which defines the permissions necessary for an action to executed
    ///
    /// \bAn example of an action class:\b
    /// \code
    ///    class user_public_read : public QObject {
    ///
    ///        Q_OBJECT
    ///
    ///    public:
    ///        user_public_read()
    ///            : QObject(nullptr) {}
    ///
    ///        void operator()(client * p_client) {
    ///            QUrl _url("https://api.spotify.com/v1/me");
    ///            QJsonDocument _doc = p_client->request(_url);
    ///            const QJsonObject _root = _doc.object();
    ///            QString _user = _root.value("id").toString();
    ///             user_public(_user);
    ///        }
    ///
    ///        permissions get_permission() const { return user_read_public; }
    ///
    ///    signals:
    ///        void user_public(const QString &);
    ///
    ///    private:
    ///        client * m_client;
    ///    };
    /// \endcode
    ///
    ///
    /// \bAnd this is how one should call \p authorize:\b
    /// \code
    /// client * _client = new client(...);
    /// ...
    /// _client->authorize<_user_public_read, _another_action>();
    /// \endcode
    template <typename ...t_actions>
    void authorize() {

        if (m_is_authorized) {
            qDebug() << "client::authorize<> - already authorized";
            return;
        }

        permissions _permissions(permissions::user_read_public);

        for ( const permissions &p : { t_actions::permission... } ) {
            _permissions += p;
        }

        m_spotify.setScope(_permissions.to_string());

        m_spotify.grant();
    }

    ///
    /// \brief authorize asks for authorization to access Spotify with no
    /// special permissions
    ///
    void authorize();

    ///
    /// \brief get_user
    /// \return
    ///
    inline const std::shared_ptr<const ent::user> get_user() const {
        return m_user;
    }

    ///
    /// \brief get_searched_tracks
    /// \return
    ///
    inline const ent::tracks & get_searched_tracks() const {
        return m_searched_tracks;
    }

    ///
    /// \brief get_playlists retrieves the playlists of the user
    /// \return
    ///
    inline const ent::playlists get_playlists() const {
        return m_playlists;
    }

    ///
    /// \brief get_message
    /// \param p_msg_id
    /// \return
    ///
    QString get_message(ent::msg_id p_msg_id);


    ///
    /// \brief request sends a request to Spotify, and returns the JSON document
    /// retrieved
    /// \param p_url the entry point of a Spotify service
    /// \return JSON document with the response
    ///
    QJsonDocument request(const QUrl & p_url);

    /// not allowed
    inline client & operator=(const client &) = delete;

    /// not allowed
    inline client & operator=(client &&) = delete;

    /// not allowed
    void *operator new[] (size_t) = delete;

    /// not allowed
    void operator delete[] (void *) = delete;

    // ---------------------------------------------
    signals:

    ///
    /// \brief error emited when an error occurs, with a string explaining what
    /// caused
    ///
    void problem(ent::msg_id);

    ///
    /// \brief authorized is emited when the user was authorized
    ///
    void authorized();


    ///
    /// \brief user_public_read is used to signalize that a \p
    /// read_user_public_action finished
    ///
    void user_public_read();

    ///
    /// \brief track_seached is used to signalize that a
    /// \p search_track_action was finished
    ///
    void track_searched_found();

    ///
    /// \brief track_searched_not_found is used to signalize that the search for
    /// tracks found no track
    ///
    void track_searched_not_found();

    ///
    /// \brief user_playlists_loaded emites when the user's playlists were
    /// loaded
    ///
    void user_playlists_loaded();

    ///
    /// \brief playlist_created is emited when a new playlist was created
    ///
    void playlist_created();

    ///
    /// \brief track_inserted is emited when a track is inserted into a playlist
    ///
    void track_inserted();

    ///
    /// \brief track_removed is emited when a track was removed from its
    /// playlist
    ///
    void track_removed();

    ///
    /// \brief playlist_removed is emited when a playlist is removed
    ///
    void playlist_removed();

    // ---------------------------------------------
    public slots:

    ///
    /// \brief user_public_read is used to start a \p read_user_public_action
    ///
    void on_read_user_public();

    ///
    /// \brief on_search_track is used to start a \p search_track_action
    ///
    void on_search_track(const QString & p_criteria);

    ///
    /// \brief on_create_local_playlist is used to create a local playlist
    /// \param p_name
    ///
    void on_create_playlist(const ent::entity::id &p_user_id, const QString & p_name);

    ///
    /// \brief on_clean_searched_tracks cleans the searched tracks cache
    ///
    void on_clean_searched_tracks();

    ///
    /// \brief on_insert_track inserts a track into a playlist
    /// \param p_track
    /// \param p_playist
    ///
    void on_insert_track(const ent::track &p_track
    , const ent::entity::id & p_playist_id);


    ///
    /// \brief on_remove_track removes a track from its playlist
    /// \param p_track_id
    /// \param p_playlist_id
    ///
    void on_remove_track(const ent::track::id & p_track_id
    , const ent::playlist::id & p_playlist_id);

    ///
    /// \brief on_remove_playlist removes an empty playlist
    /// \param p_playlist_id
    ///
    void on_remove_playlist(const ent::playlist::id & p_playlist_id);

    // ---------------------------------------------
    private slots:

    ///
    /// \brief authorizeWithBrowser is called as part of the authorization
    ///  process.
    /// \param url is the URL of the authorization
    ///
    void authorizeWithBrowser(const QUrl &url);

    ///
    /// \brief authStatusChanged called when the status of the authorization
    /// process changes
    /// \param p_status contains the status of the authorization process
    ///
    void authStatusChanged (QAbstractOAuth::Status p_status);

    ///
    /// \brief granted called when the user was authorized
    ///
    void granted();

    // ---------------------------------------------
    private:


//    ///
//    /// \brief execute demands that a certain action to be executed, providing
//    /// the necessary parameters
//    ///
//    /// \tparam t_action is an action, one define in
//    /// $DVP/spotify/common/bus/actions.h
//    /// \tparam t_types are the types of the parameters for an instance of
//    /// \p t_action to work
//    template <typename t_action, typename ...t_types>
//    void execute(t_types & ... p_params) {
//        t_action _action;
//        _action(this, p_params...);
//    }

    ///
    /// \brief m_authorizer_browser is used to authorize a user
    ///
    QWebEngineView * m_authorizer_browser;

    ///
    /// \brief m_close_browser_after_authorized defines if the browser used to
    /// authorize the user should be closed after authorization is granted
    ///
    bool m_close_browser_after_authorized;

    ///
    /// \brief m_spotify access to Spotify
    ///
    QOAuth2AuthorizationCodeFlow m_spotify;

    ///
    /// \brief m_is_granted indicates if an access to Spotify was authorized
    ///
    bool m_is_authorized;

    ///
    /// \brief m_user the only user
    ///
    std::shared_ptr<ent::user> m_user;

    ///
    /// \brief m_tracks tracks opened/used/dowloaded/played by the user
    ///
    ent::tracks m_searched_tracks;

    ///
    /// \brief m_playlists the playlists created by the user
    ///
    ent::playlists m_playlists;

    ///
    /// \brief m_messages catalog of messages
    ///
    ent::messages m_messages;

    ///
    /// \brief m_db
    ///
    per::db_local m_db;

};


    // ==> Attributes

    // ==> Functions





}
}
}



#endif
