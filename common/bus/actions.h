#ifndef spotify__common__bus__actions__H
#define spotify__common__bus__actions__H


/// \file In this file are implemented the actions that can be executed by
/// Spotify service, as described in
/// https://developer.spotify.com/documentation/web-api/reference/

/// \author Rodrigo Canellas

/// \date may/2018

// ==> Headers C++
#include <cstdint>

// ==> Qt Headers
#include <QObject>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDateTime>

#include <QDebug>


// ==> 3rd Headers

// ==> Our Headers
#include <spotify/common/bus/permissions.h>
#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>

// ==> Namespaces
using namespace std;

// ==> Macro Constants

// ==> Macro Commands

// project
namespace spotify {
// artifact
namespace common {
// layer
namespace bus {

// ==> Typedefs

// ==> Pre-Declarations

/// \brief The read_user_public struct reads public info about the current user
struct read_user_public_action;

/// \brief The search_track_action searches for tracks based on a filter
struct search_track_action;

// ==> Attributes

// ==> Functions

// ==> structes




///
/// \brief The read_user_public struct reads public info about the current user[
///
struct read_user_public_action {
    ///
    /// \brief permission the permission that this action requieres
    ///
    static const permissions permission;
};


///
/// \brief The search_track_action searches for tracks based on a filter
///
struct search_track_action {
    ///
    /// \brief permission the permission that this action requieres
    ///
    static const permissions permission;

};

///
/// \brief The create_local_playlist struct creates a local playlist
///
struct create_local_playlist_action {
    static const permissions permission;
};





}
}
}


#endif
