


QT       += core network networkauth webenginewidgets sql

######
# name of the product
TARGET = spotify.common.bus

########
#

# projects that generate libs
TEMPLATE = lib

SOURCES += \
    ../../permissions.cpp \
    ../../actions.cpp \
    ../../client.cpp


FORMS +=

HEADERS += \
    ../../permissions.h \
    ../../actions.h \
    ../../client.h

include ($(DVP)/tenacitas.configuration/qmake/common.pri)

LIBS += $$libs_dir/libspotify.common.obj.$$static_lib_ext
LIBS += $$libs_dir/libspotify.common.per.$$static_lib_ext

DISTFILES += \
    ../../../doc/search_results.json




