#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>

using namespace spotify::common;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_browser = new QWebEngineView;
    ui->layBrowser->addWidget(m_browser);

    m_client = new bus::client("6c3058ffc39c45ae98f8426ca85d8078"
                            , "c212c2515bed4ae199c485dd937f1832"
                            , m_browser
                            , this);

    QObject::connect(m_client, SIGNAL(authorized())
                     , this, SLOT(on_authorized()));

    QObject::connect(m_client, SIGNAL(problem(ent::msg_id))
                     , this, SLOT(on_problem(ent::msg_id)));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnAuthorize_clicked()
{
    m_client->authorize<bus::read_user_public_action>();
}

void MainWindow::on_authorized()
{
    ui->lblAuthorized->setText("AUTORIZADO");
    ui->btnUsuario->setEnabled(true);
}


void MainWindow::on_btnUsuario_clicked()
{
    qDebug() << "MainWindow::on_btnUsuario_clicked, calling action";

    QObject::connect(this, SIGNAL(read_user_public())
                     , m_client, SLOT(on_read_user_public()));
    QObject::connect(m_client, SIGNAL(user_public_read())
                     , this, SLOT(on_user_public_read()));

    emit read_user_public();
}

void MainWindow::on_user_public_read() {

    qDebug() << "MainWindow::on_user_public_read";
    ui->txeResponse->appendPlainText(
                QString("name = '" + m_client->get_user()->get_name() + "' "
                        + "id = '" + m_client->get_user()->get_id() + "'"));
}

void MainWindow::on_problem(spotify::common::ent::msg_id p_msg_id) {
    QMessageBox::critical(this
                          , tr("ERRO")
                          , m_client->get_message(p_msg_id));
}
