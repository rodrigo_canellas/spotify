#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWebEngineView>
#include <spotify/common/bus/client.h>

using namespace spotify::common;

namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnAuthorize_clicked();

    void on_authorized();

    void on_problem(ent::msg_id);


private:
    Ui::MainWindow *ui;
    QWebEngineView * m_browser;
    bus::client* m_client;
};

#endif // MAINWINDOW_H
