#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>

using namespace spotify::common;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_browser = new QWebEngineView;
    ui->layBrowser->addWidget(m_browser);

    m_client = new bus::client("6c3058ffc39c45ae98f8426ca85d8078"
                            , "c212c2515bed4ae199c485dd937f1832"
                            , m_browser
                            , this);

    QObject::connect(m_client, SIGNAL(authorized())
                     , this, SLOT(on_authorized()));

    QObject::connect(m_client, SIGNAL(problem(ent::msg_id))
                     , this, SLOT(on_problem(ent::msg_id)));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnAuthorize_clicked()
{
    m_client->authorize();
}

void MainWindow::on_authorized()
{
    ui->lblAuthorized->setText("AUTORIZADO");
}

void MainWindow::on_problem(spotify::common::ent::msg_id p_msg_id)
{
    ui->lblAuthorized->setText(m_client->get_message(p_msg_id));
}



