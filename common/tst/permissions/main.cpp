#include <iostream>
#include <spotify/common/bus/permissions.h>

using namespace spotify::common::bus;

///
/// \brief test1 tests if all the permission are not set by default
/// \return
///
bool test1() {
    permissions _permissions;

    return( !_permissions.is_used(permissions::playlist_modify_private) &&
            !_permissions.is_used(permissions::playlist_modify_public) &&
            !_permissions.is_used(permissions::playlist_read_collaborative) &&
            !_permissions.is_used(permissions::playlist_read_private) &&
            !_permissions.is_used(permissions::streaming) &&
            !_permissions.is_used(permissions::user_follow_modify) &&
            !_permissions.is_used(permissions::user_follow_read) &&
            !_permissions.is_used(permissions::user_library_modify) &&
            !_permissions.is_used(permissions::user_library_read) &&
            !_permissions.is_used(permissions::user_modify_playback_state) &&
            !_permissions.is_used(permissions::user_read_birthdate) &&
            !_permissions.is_used(permissions::user_read_currently_playing) &&
            !_permissions.is_used(permissions::user_read_email) &&
            !_permissions.is_used(permissions::user_read_playback_state) &&
            !_permissions.is_used(permissions::user_read_private) &&
            !_permissions.is_used(permissions::user_read_recently_played) &&
            !_permissions.is_used(permissions::user_top_read));
}

///
/// \brief test2 tests that altering one permission does not alter any other one
/// \return
///
bool test2() {
    permissions _permissions;

    _permissions.use(permissions::playlist_modify_public);

    return( !_permissions.is_used(permissions::playlist_modify_private) &&
            _permissions.is_used(permissions::playlist_modify_public) &&
            !_permissions.is_used(permissions::playlist_read_collaborative) &&
            !_permissions.is_used(permissions::playlist_read_private) &&
            !_permissions.is_used(permissions::streaming) &&
            !_permissions.is_used(permissions::user_follow_modify) &&
            !_permissions.is_used(permissions::user_follow_read) &&
            !_permissions.is_used(permissions::user_library_modify) &&
            !_permissions.is_used(permissions::user_library_read) &&
            !_permissions.is_used(permissions::user_modify_playback_state) &&
            !_permissions.is_used(permissions::user_read_birthdate) &&
            !_permissions.is_used(permissions::user_read_currently_playing) &&
            !_permissions.is_used(permissions::user_read_email) &&
            !_permissions.is_used(permissions::user_read_playback_state) &&
            !_permissions.is_used(permissions::user_read_private) &&
            !_permissions.is_used(permissions::user_read_recently_played) &&
            !_permissions.is_used(permissions::user_top_read));

}

///
/// \brief test3 tests that set and unset a permission actually puts it in its
/// initial state, and does not modify others
/// \return
///
bool test3() {
    permissions _permissions;
    _permissions.use(permissions::user_modify_playback_state);
    _permissions.unuse(permissions::user_modify_playback_state);

    return( !_permissions.is_used(permissions::playlist_modify_private) &&
            !_permissions.is_used(permissions::playlist_modify_public) &&
            !_permissions.is_used(permissions::playlist_read_collaborative) &&
            !_permissions.is_used(permissions::playlist_read_private) &&
            !_permissions.is_used(permissions::streaming) &&
            !_permissions.is_used(permissions::user_follow_modify) &&
            !_permissions.is_used(permissions::user_follow_read) &&
            !_permissions.is_used(permissions::user_library_modify) &&
            !_permissions.is_used(permissions::user_library_read) &&
            !_permissions.is_used(permissions::user_modify_playback_state) &&
            !_permissions.is_used(permissions::user_read_birthdate) &&
            !_permissions.is_used(permissions::user_read_currently_playing) &&
            !_permissions.is_used(permissions::user_read_email) &&
            !_permissions.is_used(permissions::user_read_playback_state) &&
            !_permissions.is_used(permissions::user_read_private) &&
            !_permissions.is_used(permissions::user_read_recently_played) &&
            !_permissions.is_used(permissions::user_top_read));

}

///
/// \brief test4 tests if the string assembled is correct, with one permission
/// set
/// \return
///
bool test4() {
    permissions _permissions;

    _permissions.use(permissions::user_read_private);

    if (_permissions.to_string() != "user-read-private") {
        std::cerr << "_permissions.to_string() = '"
                  << _permissions.to_string().toStdString()
                  << "'"
                  << std::endl;

        return false;
    }
    return true;
}

///
/// \brief test5 tests if after setting 3 permissions, all the others remain
/// the same
/// \return
///
bool test5() {
    permissions _permissions;

    _permissions.use(permissions::user_read_private);
    _permissions.use(permissions::user_library_read);
    _permissions.use(permissions::user_top_read);

    return( !_permissions.is_used(permissions::playlist_modify_private) &&
            !_permissions.is_used(permissions::playlist_modify_public) &&
            !_permissions.is_used(permissions::playlist_read_collaborative) &&
            !_permissions.is_used(permissions::playlist_read_private) &&
            !_permissions.is_used(permissions::streaming) &&
            !_permissions.is_used(permissions::user_follow_modify) &&
            !_permissions.is_used(permissions::user_follow_read) &&
            !_permissions.is_used(permissions::user_library_modify) &&
            _permissions.is_used(permissions::user_library_read) &&
            !_permissions.is_used(permissions::user_modify_playback_state) &&
            !_permissions.is_used(permissions::user_read_birthdate) &&
            !_permissions.is_used(permissions::user_read_currently_playing) &&
            !_permissions.is_used(permissions::user_read_email) &&
            !_permissions.is_used(permissions::user_read_playback_state) &&
            _permissions.is_used(permissions::user_read_private) &&
            !_permissions.is_used(permissions::user_read_recently_played) &&
            _permissions.is_used(permissions::user_top_read));
}

///
/// \brief test6 tests if setting 3 permissions, and unseting 1, dont mess with
/// the others
///
/// \return
///
bool test6() {
    permissions _permissions;

    _permissions.use(permissions::user_read_private);
    _permissions.use(permissions::user_library_read);
    _permissions.use(permissions::user_top_read);

    _permissions.unuse(permissions::user_library_read);

    return( !_permissions.is_used(permissions::playlist_modify_private) &&
            !_permissions.is_used(permissions::playlist_modify_public) &&
            !_permissions.is_used(permissions::playlist_read_collaborative) &&
            !_permissions.is_used(permissions::playlist_read_private) &&
            !_permissions.is_used(permissions::streaming) &&
            !_permissions.is_used(permissions::user_follow_modify) &&
            !_permissions.is_used(permissions::user_follow_read) &&
            !_permissions.is_used(permissions::user_library_modify) &&
            !_permissions.is_used(permissions::user_library_read) &&
            !_permissions.is_used(permissions::user_modify_playback_state) &&
            !_permissions.is_used(permissions::user_read_birthdate) &&
            !_permissions.is_used(permissions::user_read_currently_playing) &&
            !_permissions.is_used(permissions::user_read_email) &&
            !_permissions.is_used(permissions::user_read_playback_state) &&
            _permissions.is_used(permissions::user_read_private) &&
            !_permissions.is_used(permissions::user_read_recently_played) &&
            _permissions.is_used(permissions::user_top_read));

}

///
/// \brief test7 tests if the string assembled after setting 4 permissions,
/// including the first and the last one, is correct
/// \return
///
bool test7() {
    permissions _permissions;
    _permissions.use(permissions::user_top_read);
    _permissions.use(permissions::playlist_modify_public);
    _permissions.use(permissions::user_follow_modify);
    _permissions.use(permissions::streaming);

    QString _str = std::move(_permissions.to_string());

    if (_str != "user-top-read playlist-modify-public "
            "user-follow-modify streaming") {
        std::cerr << "_permissions.to_string() = '"
                  << _permissions.to_string().toStdString()
                  << "'"
                  << std::endl;
        return false;
    }
    return true;
}

#define run(t) std::cout << #t << (t() ? " OK" : " FAILED") << std::endl

int main() {

    run(test1);
    run(test2);
    run(test3);
    run(test4);
    run(test5);
    run(test6);
    run(test7);
}
