#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWebEngineView>
#include <spotify/common/bus/actions.h>
#include <spotify/common/bus/client.h>
#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/messages.h>

using namespace spotify::common;

namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnAuthorize_clicked();




    void on_btnUsuario_clicked();

    void on_btnPesquisa_clicked();

    void on_btnLimpa_clicked();

    void on_tabTracks_cellDoubleClicked(int row, int column);


    // slots to receive response of actions

    void on_problem(ent::msg_id);

    void on_authorized();

    void on_user_public_read();

    void on_track_searched_found();

    void on_track_searched_not_found();



signals:


    // signals to execute actions

    void read_user_public();

    void search_track(const QString & p_criteria);

private:
    Ui::MainWindow *ui;
    QWebEngineView * m_browser;
    QWebEngineView * m_player;
    bus::client * m_client;
};

#endif // MAINWINDOW_H
