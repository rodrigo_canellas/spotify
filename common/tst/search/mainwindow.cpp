#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>

using namespace spotify::common;
using namespace spotify::common;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_browser = new QWebEngineView;
    ui->layBrowser->addWidget(m_browser);

    m_client = new bus::client("6c3058ffc39c45ae98f8426ca85d8078"
                          , "c212c2515bed4ae199c485dd937f1832"
                          , m_browser
                          , this);

    QObject::connect(m_client, SIGNAL(authorized())
                     , this, SLOT(on_authorized()));

    QObject::connect(m_client, SIGNAL(problem(ent::msg_id))
                     , this, SLOT(on_problem(ent::msg_id)));


    ui->tabTracks->setColumnCount(3);
    QStringList _header_lbls;
    _header_lbls.append("Id");
    _header_lbls.append("Name");
    _header_lbls.append("Url");

    ui->tabTracks->setHorizontalHeaderLabels(_header_lbls);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnAuthorize_clicked()
{
    m_client->authorize<bus::read_user_public_action>();
}

void MainWindow::on_authorized()
{
    ui->lblAuthorized->setText("AUTORIZADO");
    ui->btnUsuario->setEnabled(true);
}


void MainWindow::on_btnUsuario_clicked()
{
    qDebug() << "MainWindow::on_btnUsuario_clicked, calling action";

    QObject::connect(this, SIGNAL(read_user_public())
                     , m_client, SLOT(on_read_user_public()));

    QObject::connect(m_client, SIGNAL(user_public_read())
                     , this, SLOT(on_user_public_read()));


    emit read_user_public();
}

void MainWindow::on_btnPesquisa_clicked()
{
    QObject::connect(this, SIGNAL(search_track(QString))
                     , m_client, SLOT(on_search_track(QString)));

    QObject::connect(m_client, SIGNAL(track_searched_found())
                     , this, SLOT(on_track_searched_found()));
    QObject::connect(m_client, SIGNAL(track_searched_not_found())
                     , this, SLOT(on_track_searched_not_found()));

    emit search_track(ui->txtPesquisa->text());

}


void MainWindow::on_user_public_read() {


    qDebug() << "MainWindow::on_user_public_read";
    ui->lblUsuario->setText(QString("name = '"
                                    + m_client->get_user()->get_name()
                                    + "' "

                                    + "id = '"
                                    + m_client->get_user()->get_id()
                                    + "'"));
    ui->btnPesquisa->setEnabled(true);
    ui->txtPesquisa->setEnabled(true);
}



void MainWindow::on_track_searched_found() {
    qDebug() << "MainWindow::on_track_searched";

    using namespace spotify::common::ent;

    const tracks & _tracks = m_client->get_searched_tracks();

    tracks::const_iterator _end = _tracks.end();
    for (tracks::const_iterator _ite = _tracks.begin();
         _ite != _end;
         ++_ite) {
        ui->tabTracks->
                setItem(ui->tabTracks->rowCount() - 1
                        , 0
                        , new QTableWidgetItem(_ite->get_id()));

        ui->tabTracks->setItem(
                    ui->tabTracks->rowCount() - 1
                    , 1
                    , new QTableWidgetItem(_ite->get_name()));

        ui->tabTracks->
                setItem(ui->tabTracks->rowCount() - 1
                        , 2
                        , new QTableWidgetItem(_ite->get_url().toString()));
        ui->tabTracks->insertRow( ui->tabTracks->rowCount() );
    }
}




void MainWindow::on_btnLimpa_clicked()
{
    ui->tabTracks->clearContents();
    ui->tabTracks->setRowCount(1);
}


void MainWindow::on_track_searched_not_found()
{
    QMessageBox::information(this
                             , tr("INFO")
                             , tr("Nenhuma música foi encontrada"));
}

void MainWindow::on_problem(spotify::common::ent::msg_id p_msg_id) {
    QMessageBox::critical(this
                          , tr("ERRO")
                          , m_client->get_message(p_msg_id));
}
