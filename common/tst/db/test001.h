#ifndef TEST001_H
#define TEST001_H

#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

using namespace spotify::common;

///
/// \brief The test001 struct tests correct database creation
///
struct test001 {
    struct sender : public per::i_sender {
        QJsonDocument request(const QUrl & /*p_url*/) {
            return QJsonDocument();
        }
    };

    QString name() const { return "test001"; }
    bool operator()() {
        try {
            sender _sender;
            per::db_local _db(&_sender);
            return true;
        }
        catch (std::exception & _ex) {
            qDebug() << "ERROR! " << _ex.what();
        }
        return false;
    }
};

#endif // TEST1_H
