#ifndef TEST010_H
#define TEST010_H

#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

using namespace spotify::common;

///
/// \brief The test010 struct tests getting a user public data from a JSON
///
struct test010 {
    struct sender : public per::i_sender {
        QJsonDocument request(const QUrl & /*p_url*/) {
            QString _contents("{"
                              "\"birthdate\": \"2003-05-05\", "
                              "\"country\": \"BR\", "
                              "\"display_name\": \"Jose das Couves\", "
                              "\"email\": \"jose.das.couves@gmail.com\", "
                              "\"external_urls\": { "
                              "\"spotify\": \"https://open.spotify.com/user/jose.das.couves\" "
                              "},"
                              "\"followers\": { "
                              "\"href\": null, "
                              "\"total\": 0 "
                              "}, "
                              "\"href\": \"https://api.spotify.com/v1/users/jose.das.couves\", "
                              "\"id\": \"jose.das.couves\", "
                              " \"images\": [], "
                              "\"product\": \"premium\", "
                              "\"type\": \"user\", "
                              "\"uri\": \"spotify:user:jose.das.couves\" "
                              "} "
                              );
            QJsonDocument _doc = QJsonDocument::fromJson(_contents.toUtf8());
            if (_doc.isEmpty()) {
                throw std::runtime_error("JSON para test010 vazio");
            }
            if (!_doc.isObject()) {
                throw std::runtime_error("JSON para test010 inválido");
            }
            return _doc;
        }
    };

    QString name() const { return "test010"; }
    bool operator()() {
        try {
            sender _sender;
                per::db_local _db(&_sender);
            ent::user _user;
            if (!_db.get_user_public(_user)) {
                return false;
            }
            if (_user.get_id() != "jose.das.couves") {
                qDebug() << "######## " << name() << ": get_id failed";
                return false;
            }
            if (_user.get_name() != "Jose das Couves") {
                qDebug() << "######## " << name() << ": get_mame failed";
                return false;
            }
            return true;
        }
        catch (std::exception & _ex) {
            qDebug() << "######## " << name() << ": ERROR! " << _ex.what();
        }
        return false;
    }
};
#endif // TEST010_H
