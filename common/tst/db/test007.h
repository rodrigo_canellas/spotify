#ifndef TEST007_H
#define TEST007_H

#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

#include <spotify/common/tst/db/common.h>


///
/// \brief test7 insertion of a track into a playlist
/// \return
///
struct test007{
    struct sender : public per::i_sender {
        QJsonDocument request(const QUrl & /*p_url*/) {
            return QJsonDocument();
        }
    };
    QString name() const { return "test007"; }
    bool operator()() {
        try {



            sender _sender;
            per::db_local _db(&_sender);

            ent::user _user ("Maria das Neves", gen_dummy_id());

            if (!_db.insert_user(_user)) {
                qDebug() << "Erro inserindo usuário" ;
                return false;
            }

            ent::playlist _playlist;
            if (!_db.insert_playlist(_user.get_id(), "playlist 2", _playlist)) {
                qDebug() << "Erro criando playlist" ;
                return false;
            }

            ent::entity::id _track_id = gen_dummy_id();

            QUrl _url ("http://" + _track_id + ".com");

            ent::track _track(gen_dummy_id(), _url, "Surfing with an alien");

            if (!_db.insert_track(_track, _playlist.get_id())) {
                qDebug() << "Erro inserindo trilha" ;
            }

            qDebug() << "playlist: id = '" << _playlist.get_id() << "'"
                     << ", name = '" << _playlist.get_name() << "'"
                     << ", url = '" << _playlist.get_url().toString() << "'"
                     << ", type = '"
                     << _playlist.type_to_str(_playlist.get_type()) << "'"
                     << ", track: id = '" << _track.get_id() << "'"
                     << ", url = '" << _track.get_url().toString() << "'"
                     << ", name = '" << _track.get_name() << "'"
                     << ", type = '"
                     << _track.type_to_str(_track.get_type()) << "'"
                        ;

            return true;

        }
        catch (std::exception & _ex) {
            qDebug() << "ERROR! " << _ex.what();
        }
        return false;

    }
};

#endif // TEST7_H
