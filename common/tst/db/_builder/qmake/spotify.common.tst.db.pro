
#QT += core
QT       += core sql



TARGET = spotify.common.tst.db
TEMPLATE = app


SOURCES += \
    ../../main.cpp

HEADERS += \
    ../../common.h \
    ../../test001.h \
    ../../test002.h \
    ../../test003.h \
    ../../test004.h \
    ../../test005.h \
    ../../test006.h \
    ../../test007.h \
    ../../test008.h \
    ../../test009.h \
    ../../test010.h \
    ../../test011.h

FORMS += \



include ($(DVP)/configuration/qmake/common.pri)

LIBS += $$libs_dir/libspotify.common.$$static_lib_ext -lgtest

message("LIBS = "$$LIBS)

#LIBS += -lspotify.common



