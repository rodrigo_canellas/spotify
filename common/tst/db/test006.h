#ifndef TEST006_H
#define TEST006_H

#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

#include <spotify/common/tst/db/common.h>


///
/// \brief The test6 struct playlist creation
///
struct test006 {
    struct sender : public per::i_sender {
        QJsonDocument request(const QUrl & /*p_url*/) {
            return QJsonDocument();
        }
    };
    QString name() const {return "test006";}

    bool operator()() {
        try {
            using namespace std;

            sender _sender;
            per::db_local _db(&_sender);

            ent::user _user ("João das Neves", gen_dummy_id());

            if (!_db.insert_user(_user)) {
                qDebug() << "Erro inserindo usuário" ;
                return false;
            }

            ent::playlist _playlist;
            if (_db.insert_playlist(_user.get_id(), "playlist 1", _playlist)) {
                qDebug() << "playlist: id = '" << _playlist.get_id() << "'"
                          << ", name = '" << _playlist.get_name() << "'"
                          << ", url = '" << _playlist.get_url().toString() << "'"
                          << ", type = '"
                          << _playlist.type_to_str(_playlist.get_type()) << "'";

                return true;

            }
        }
        catch (std::exception & _ex) {
            qDebug() << "ERROR! " << _ex.what();
        }
        return false;

    }
};

#endif // TEST6_H
