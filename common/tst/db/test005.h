#ifndef TEST005_H
#define TEST005_H

#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

#include <spotify/common/tst/db/common.h>


///
/// \brief The test5 struct user creation
///
struct test005 {
    struct sender : public per::i_sender {
        QJsonDocument request(const QUrl & /*p_url*/) {
            return QJsonDocument();
        }
    };

    QString name() const {return "test005";}

    bool operator()() {
        try {
            using namespace std;

            sender _sender;
            per::db_local _db(&_sender);

            ent::user _user ("João das Neves", gen_dummy_id());

            if (!_db.insert_user(_user)) {
                qDebug() << "Erro inserindo usuário" ;
                return false;
            }

            return true;

        }
        catch (std::exception & _ex) {
            qDebug() << "ERROR! " << _ex.what();
        }
        return false;

    }
};
#endif // TEST5_H
