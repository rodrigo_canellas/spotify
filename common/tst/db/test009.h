#ifndef TEST009_H
#define TEST009_H

#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

using namespace spotify::common;

///
/// \brief The test009 struct tests incorrect database creation
///
struct test009 {
    struct sender : public per::i_sender {
        QJsonDocument request(const QUrl & /*p_url*/) {
            return QJsonDocument();
        }
    };

    QString name() const { return "test009"; }
    bool operator()() {
        try {
            sender _sender;
            per::db_local _db(&_sender, "/var/tmp/xpto1010");
            return false;
        }
        catch (std::exception & _ex) {
            qDebug() << "ERROR! " << _ex.what();
        }
        return true;
    }
};

#endif // TEST9_H
