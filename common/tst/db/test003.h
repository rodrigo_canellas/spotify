#ifndef TEST003_H
#define TEST003_H

#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

#include <spotify/common/tst/db/common.h>

///
/// \brief The test3 check a non existing playlist
///
struct test003 {
    struct sender : public per::i_sender {
        QJsonDocument request(const QUrl & /*p_url*/) {
            return QJsonDocument();
        }
    };

    QString name() const { return "test003"; }

    bool operator()() {
        using namespace std;
        try {
            sender _sender;
            per::db_local _db(&_sender);

            ent::entity::id _playlist_id = gen_dummy_id();
            bool _playlist_exists = false;
            if (!_db.exists_playlist(_playlist_id, _playlist_exists)) {
                qDebug() << "Falha na busca por playlist";
                return false;
            }

            if (_playlist_exists) {
                qDebug() << "Playlist consta como existente, quando não existe";
                return false;
            }

            if (!_playlist_exists) {
                qDebug() << "Playlist '" << _playlist_id
                     << "' não encontrado";
                return true;
            }
        }
        catch (std::exception & _ex) {
            qDebug() << "ERROR! " << _ex.what();
        }
        return false;
    }
};
#endif // TEST3_H
