#ifndef TEST2_H
#define TEST2_H

#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <spotify/common/tst/db/common.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

using namespace spotify::common;

///
/// \brief The test002 check a non existing user
///
struct test002 {
    struct sender : public per::i_sender {
        QJsonDocument request(const QUrl & /*p_url*/) {
            return QJsonDocument();
        }
    };
    QString name() const { return "test002"; }
    bool operator()() {
        using namespace std;
        try {
            sender _sender;
            per::db_local _db(&_sender);

            ent::entity::id _user_id = gen_dummy_id();
            bool _user_exists = false;
            if (!_db.exists_user(_user_id, _user_exists)) {
                qDebug() << "Falha na busca por usuário" << endl;
                return false;
            }

            if (_user_exists) {
                qDebug() << "Usuário consta como existente, quando não existe" << endl;
                return false;
            }

            if (!_user_exists) {
                qDebug() << "Usuário '" << _user_id
                     << "' não encontrado" << endl;
                return true;
            }
        }
        catch (std::exception & _ex) {
            qDebug() << "ERROR! " << _ex.what();
        }
        return false;
    }
};
#endif // TEST2_H
