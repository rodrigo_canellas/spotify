#include <iostream>
#include <thread>
#include <chrono>

#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

#include <spotify/common/tst/db/common.h>

#include <spotify/common/tst/db/test001.h>
#include <spotify/common/tst/db/test002.h>
#include <spotify/common/tst/db/test003.h>
#include <spotify/common/tst/db/test004.h>
#include <spotify/common/tst/db/test005.h>
#include <spotify/common/tst/db/test006.h>
#include <spotify/common/tst/db/test007.h>
#include <spotify/common/tst/db/test008.h>
#include <spotify/common/tst/db/test009.h>
#include <spotify/common/tst/db/test010.h>
#include <spotify/common/tst/db/test011.h>

#include <gtest/gtest.h>

using namespace spotify::common;

struct sender : public per::i_sender {
    QJsonDocument request(const QUrl & /*p_url*/) {
        return QJsonDocument();
    }
};



TEST(DBCreation, CorrectCreation) {
    ASSERT_EQ(true, test001()());
}
TEST(DBCreation, InCorrectCreation) {
    ASSERT_EQ(true, test009()());
}

TEST(DBUser, NonExisting) {
    ASSERT_EQ(true, test002()());
}
TEST(DBUser, CorrectCreation) {
    ASSERT_EQ(true, test005()());
}
TEST(DBUser, RetrivePublicData) {
    ASSERT_EQ(true, test010()());
}
TEST(DBUser, RetriveInsertedData) {
    ASSERT_EQ(true, test011()());
}


TEST(DBPlaylist, NonExisting) {
    ASSERT_EQ(true, test003()());
}
TEST(DBPlaylist, CorrectCreation) {
    ASSERT_EQ(true, test006()());
}

TEST(DBTrack, NonExisting) {
    ASSERT_EQ(true, test004()());
}
TEST(DBTrack, CorrectCreation) {
    ASSERT_EQ(true, test007()());
}
TEST(DBTrack, CorrectRetrive) {
    ASSERT_EQ(true, test008()());
}




template <typename t_test>
void run(t_test && p_test) {

    using namespace std;

    cout << "\n\n################### " << p_test.name() << " STARTED\n" << endl;

    bool _rc = p_test();
    cerr <<  endl << p_test.name() ;
    if (_rc) {
        cerr << " OK!!";
    }
    else {
        cerr << " FAILED!!";
    }
    cerr << endl ;
    cout << endl << p_test.name() << " FINISHED" << endl;
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
     return RUN_ALL_TESTS();
}
