#ifndef TEST008_H
#define TEST008_H


#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

#include <spotify/common/tst/db/common.h>


///
/// \brief The test8 struct tests retireve of tracks
///
struct test008{
    struct sender : public per::i_sender {
        QJsonDocument request(const QUrl & /*p_url*/) {
            return QJsonDocument();
        }
    };
    QString name() const { return "test008"; }
    bool operator()() {
        try {

            sender _sender;
            per::db_local _db(&_sender);

            ent::user _user ("Joana das Neves", "rodrigocanellas");

            if (!_db.insert_user(_user)) {
                qDebug() << "Erro inserindo usuário" ;
                return false;
            }


            // playlist da praia
            {
                ent::playlist _playlist;
                if (!_db.insert_playlist(_user.get_id(), "playlist da praia"
                                         , _playlist)) {
                    qDebug() << "Erro criando playlist" ;
                    return false;
                }

                {
                    ent::entity::id _track_id = gen_dummy_id();

                    QUrl _url ("http://" + _track_id + ".com");

                    ent::track _track(gen_dummy_id(), _url, "Surfing with an alien");

                    if (!_db.insert_track(_track, _playlist.get_id())) {
                        qDebug() << "Erro inserindo trilha '"
                                  << _track_id << "'"
                                  ;
                    }
                }

                {
                    ent::entity::id _track_id = gen_dummy_id();

                    QUrl _url ("http://" + _track_id + ".com");

                    ent::track _track(gen_dummy_id(), _url, "Bela praia");

                    if (!_db.insert_track(_track, _playlist.get_id())) {
                        qDebug() << "Erro inserindo trilha '"
                                  << _track_id << "'"
                                  ;
                    }
                }

                {
                    ent::entity::id _track_id = gen_dummy_id();

                    QUrl _url ("http://" + _track_id + ".com");

                    ent::track _track(gen_dummy_id(), _url, "Gaivota voadora");

                    if (!_db.insert_track(_track, _playlist.get_id())) {
                        qDebug() << "Erro inserindo trilha '"
                                  << _track_id << "'";
                    }
                }
            }

            // playlist da montanha
            {
                ent::playlist _playlist;
                if (!_db.insert_playlist(_user.get_id(), "playlist da montanha"
                                         , _playlist)) {
                    qDebug() << "Erro criando playlist" ;
                    return false;
                }

                {
                    ent::entity::id _track_id = gen_dummy_id();

                    QUrl _url ("http://" + _track_id + ".com");

                    ent::track _track(gen_dummy_id(), _url, "Fogo alto");

                    if (!_db.insert_track(_track, _playlist.get_id())) {
                        qDebug() << "Erro inserindo trilha '"
                                  << _track_id << "'";
                    }
                }

                {
                    ent::entity::id _track_id = gen_dummy_id();

                    QUrl _url ("http://" + _track_id + ".com");

                    ent::track _track(gen_dummy_id(), _url, "Noite fria");

                    if (!_db.insert_track(_track, _playlist.get_id())) {
                        qDebug() << "Erro inserindo trilha '"
                                  << _track_id << "'";
                    }
                }

                {
                    ent::entity::id _track_id = gen_dummy_id();

                    QUrl _url ("http://" + _track_id + ".com");

                    ent::track _track(gen_dummy_id(), _url, "Mais vinho");

                    if (!_db.insert_track(_track, _playlist.get_id())) {
                        qDebug() << "Erro inserindo trilha '"
                                  << _track_id << "'";
                    }
                }
            }

            ent::playlists _playlists;
            if (!_db.load_playlists(_user.get_id(), _playlists)) {
                qDebug() << "Erro carregando playlists" ;
            }

            ent::playlists::const_iterator _end_playlists = _playlists.end();

            for (ent::playlists::const_iterator _ite_p = _playlists.begin();
                 _ite_p != _end_playlists;
                 ++_ite_p) {
                qDebug() << "playlist: id = '" << _ite_p->get_id()
                     << "', "

                     << "name = '" << _ite_p->get_name() << "', "

                     << "url = '" << _ite_p->get_url().toString()
                     << "'" ;
                const ent::tracks & _tracks = _ite_p->get_tracks();
                ent::tracks::const_iterator _end_tracks = _tracks.end();
                for (ent::tracks::const_iterator _ite_t = _tracks.begin();
                     _ite_t != _end_tracks;
                     ++_ite_t) {
                    qDebug() << "\ttrack: id = '" << _ite_t->get_id()
                         << "', "

                         << "name = '" << _ite_t->get_name()
                         << "', "

                         << "url = '" << _ite_t->get_url().toString()
                         << "'";
                }
            }

            return true;

        }
        catch (std::exception & _ex) {
            qDebug() << "ERROR! " << _ex.what();
        }
        return false;

    }
};
#endif // TEST8_H
