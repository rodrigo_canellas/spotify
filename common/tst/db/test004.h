#ifndef TEST004_H
#define TEST004_H


#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

#include <spotify/common/tst/db/common.h>


///
/// \brief The test4 check a non existing track
///
struct test004 {
    struct sender : public per::i_sender {
        QJsonDocument request(const QUrl & /*p_url*/) {
            return QJsonDocument();
        }
    };
    QString name() const { return "test004"; }

    bool operator()() {
        using namespace std;
        try {
            sender _sender;
            per::db_local _db(&_sender);

            ent::entity::id _track_id = gen_dummy_id();
            bool _track_exists = false;
            if (!_db.exists_track(_track_id, _track_exists)) {
                qDebug() << "Falha na busca por track" ;
                return false;
            }

            if (_track_exists) {
                qDebug() << "track consta como existente, quando não existe" ;
                return false;
            }

            if (!_track_exists) {
                qDebug() << "track '" << _track_id << "' não encontrado" ;
                return true;
            }
        }
        catch (std::exception & _ex) {
            qDebug() << "ERROR! " << _ex.what();
        }
        return false;
    }
};

#endif // TEST4_H
