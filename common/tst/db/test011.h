#ifndef TEST011_H
#define TEST011_H

#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>
#include <spotify/common/per/db_local.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

using namespace spotify::common;

///
/// \brief The test011 struct tests getting an existing user from the local db
///
struct test011 {
    struct sender : public per::i_sender {
        QJsonDocument request(const QUrl & /*p_url*/) {
            QString _contents("{"
                              "\"birthdate\": \"2003-05-05\", "
                              "\"country\": \"BR\", "
                              "\"display_name\": \"Maria das Couves\", "
                              "\"email\": \"maria.das.couves@gmail.com\", "
                              "\"external_urls\": { "
                              "\"spotify\": \"https://open.spotify.com/user/maria.das.couves\" "
                              "},"
                              "\"followers\": { "
                              "\"href\": null, "
                              "\"total\": 0 "
                              "}, "
                              "\"href\": \"https://api.spotify.com/v1/users/maria.das.couves\", "
                              "\"id\": \"maria.das.couves\", "
                              " \"images\": [], "
                              "\"product\": \"premium\", "
                              "\"type\": \"user\", "
                              "\"uri\": \"spotify:user:maria.das.couves\" "
                              "} "
                              );
            QJsonDocument _doc = QJsonDocument::fromJson(_contents.toUtf8());
            if (_doc.isEmpty()) {
                throw std::runtime_error("JSON para test011 vazio");
            }
            if (!_doc.isObject()) {
                throw std::runtime_error("JSON para test011 inválido");
            }
            return _doc;
        }
    };

    QString name() const { return "test011"; }
    bool operator()() {
        try {
            sender _sender;
                per::db_local _db(&_sender);
            ent::user _user;
            if (!_db.get_user_public(_user)) {
                return false;
            }
            if (_user.get_id() != "maria.das.couves") {
                qDebug() << "######## " << name() << ": get_id failed";
                return false;
            }
            if (_user.get_name() != "Maria das Couves") {
                qDebug() << "######## " << name() << ": get_mame failed";
                return false;
            }
            if (!_db.insert_user(_user)) {
                qDebug() << "######## " << name() << ": insert_user failed";
            }

            {
                ent::user _user_found;
                bool _found;

                if (!_db.get_user(_user.get_id()
                                  , _found
                                  , _user_found)) {
                    qDebug() << "######## " << name() << ": get_user failed";
                    return false;
                }

                if (_user_found.get_id() != _user.get_id()) {
                    qDebug() << "######## " << name() << ": user id found "
                                "not equal to inserted";
                    return false;
                }
                if (_user_found.get_name() != _user.get_name()) {
                    qDebug() << "######## " << name() << ": user id name "
                                "not equal to inserted";
                    return false;
                }
            }
            return true;
        }
        catch (std::exception & _ex) {
            qDebug() << "######## " << name() << ": ERROR! " << _ex.what();
        }
        return false;
    }
};

#endif // TEST011_H
