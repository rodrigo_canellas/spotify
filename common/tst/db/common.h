#ifndef COMMON_H
#define COMMON_H

#include <thread>
#include <chrono>

#include <spotify/common/ent/entity.h>

using namespace spotify::common;

ent::entity::id gen_dummy_id() {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    return ent::entity::id(QString::number(QDateTime::currentMSecsSinceEpoch()));
}

#endif // COMMON_H
