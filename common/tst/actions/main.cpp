#include <iostream>
#include <spotify/common/bus/actions.h>
#include <spotify/common/bus/permissions.h>

using namespace spotify::common::bus;

///
/// \brief test1 tests if the default value of false is set
/// \return
///
bool test1() {

    actions _actions;

    return !_actions.is_used(actions::playlist_local_track_add);
}

///
/// \brief test2 tests if the default value of no permission is set
/// \return
///
bool test2() {
    actions _actions;

    return (_actions.get_permissions(actions::playlist_local_track_add) ==
            permissions());

}

///
/// \brief test3
/// \return
///
bool test3() {
    actions _actions;

    QString _s1 = _actions.get_permissions(actions::user_email_read).
            to_string();



    QString _s2 = permissions(permissions::user_read_email).to_string();
    std::cerr << "test3, s1 = '" << _s1.toStdString() << "'"
              << ", s2 = '" << _s2.toStdString() << "'" << std::endl;
    return _s1 == _s2;

}

#define run(t) std::cout << #t << (t() ? " OK" : " FAILED") << std::endl

int main() {

    run(test1);
    run(test2);
    run(test3);
}
