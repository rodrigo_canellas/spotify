


QT       += core network networkauth webenginewidgets sql

######
# name of the product
TARGET = spotify.common

########
#

VERSION=0.0.3

# projects that generate libs
TEMPLATE = lib

SOURCES += \
    ../../bus/actions.cpp \
    ../../bus/client.cpp \
    ../../bus/permissions.cpp \
    ../../ent/user.cpp \
    ../../per/db_local.cpp

FORMS +=

HEADERS += \
    ../../bus/actions.h \
    ../../bus/client.h \
    ../../bus/permissions.h \
    ../../ent/entity.h \
    ../../ent/playlist.h \
    ../../ent/track.h \
    ../../ent/user.h \
    ../../per/db_local.h \
    ../../ent/messages.h

include ($(DVP)/configuration/qmake/common.pri)





