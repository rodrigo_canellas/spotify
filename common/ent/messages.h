#ifndef __spotify____common__ent__code_message__H
#define __spotify____common__ent__code_message__H


/// \file

/// \author

/// \date

// ==> Headers C++
#include <cstdint>
#include <utility>
#include <map>

// ==> Qt Headers
#include <QString>

// ==> 3rd Headers

// ==> Our Headers


// ==> Namespaces
using namespace std;

// ==> Macro Constants

// ==> Macro Commands


// project
namespace spotify {
// artifact
namespace common {
// layer
namespace ent {

// ==> Typedefs

///
/// \brief code is the message code type
///
typedef uint32_t msg_id;

///
/// \brief language identifier of the language, such as "en" or "pt-br"
///
typedef QString language;

///
/// \brief messages associates messages identifiers to a text
///
typedef std::map<msg_id, QString> messages;

// ==> Pre-Declarations

// ==> Attributes

// ==> Functions

// ==> Classes

// ==> Attributes

static const msg_id error_loading_user_public = 1;
static const msg_id error_authorizing_user = 2;
static const msg_id error_searching_for_tracks = 3;
static const msg_id error_loading_playlists = 4;
static const msg_id error_creating_playlist = 5;
static const msg_id error_inserting_track_into_playlist = 6;
static const msg_id error_removing_track_from_playlist = 7;
static const msg_id error_removing_playlist = 8;


// ==> Functions


}
}
}


#endif
