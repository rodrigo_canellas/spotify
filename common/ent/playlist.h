#ifndef spotify__common__ent__playlist__H
#define spotify__common__ent__playlist__H


/// \file

/// \author

/// \date

// ==> Headers C++
#include <cstdint>

// ==> Qt Headers
#include <QString>
#include <QUrl>

// ==> 3rd Headers

// ==> Our Headers
#include <spotify/common/ent/entity.h>
#include <spotify/common/ent/track.h>

// ==> Namespaces
using namespace std;

// ==> Macro Constants

// ==> Macro Commands

// project
namespace spotify {
// artifact
namespace common {
// layer
namespace ent {



// ==> Pre-Declarations

// ==> Attributes

// ==> Functions

// ==> Classes

///
/// \brief The playlist class implements a playlist of tracks
///
class playlist : public spotify::common::ent::entity  {

    // ---------------------------------------------

public:


    /// not allowed
    inline playlist()
        : ent::entity() {}

    ///
    /// \brief playlist constructor by copying
    /// \param p_id
    /// \param p_url
    /// \param p_name
    ///
    inline playlist(const id & p_id
                    , const QUrl & p_url
                    , const QString & p_name)
        : entity(p_id, p_url, p_name, type::playlist)
        , m_tracks(){}

    ///
    /// \brief playlist constructor by moving
    /// \param p_id
    /// \param p_url
    /// \param p_name
    ///
    inline playlist( id && p_id
                     , QUrl && p_url
                     , QString && p_name)
        : entity(std::move(p_id)
                 , std::move(p_url)
                 , std::move(p_name)
                 , type::playlist)
        , m_tracks() {}


    ///
    /// \brief playlist copy constructor
    /// \param p_playlist
    ///
    playlist(const playlist & p_playlist)
        : entity(p_playlist)
        , m_tracks(p_playlist.m_tracks){}

    ///
    /// \brief playlist
    /// \param p_playlist
    ///
    playlist(playlist && p_playlist)
        : entity(std::move(p_playlist))
        , m_tracks(std::move(p_playlist.m_tracks)){}


    ///
    /// \brief ~playlist
    ///
    inline ~playlist() {}

    ///
    /// \brief add adds a track, by copy
    /// \param p_track
    ///
    inline void add(const track & p_track) {
        m_tracks.emplace(p_track);
    }

    ///
    /// \brief add adds a track, by move
    /// \param p_track
    ///
    inline void add(track && p_track)  {
        m_tracks.emplace(std::move(p_track));
    }

    ///
    /// \brief add adss a set of tracks to this playlist
    /// \param p_tracks
    ///
    inline void add(const tracks & p_tracks) {
        ent::merge(m_tracks, p_tracks);
    }

    ///
    /// \brief set setd the tracks to this playlist
    /// \param p_tracks
    ///
    inline void set(tracks && p_tracks) {
        m_tracks = std::move(p_tracks);
    }

    ///
    /// \brief get_tracks
    /// \return
    ///
    inline const tracks & get_tracks() const {
        return m_tracks;
    }


    ///
    /// \brief operator = move
    /// \param p_playlist
    ///
    inline playlist & operator=(playlist && p_playlist) {
        if (this != &p_playlist) {
            entity::operator =(std::move(p_playlist));
            m_tracks = std::move(p_playlist.m_tracks);            
        }
        return *this;
    }

    ///
    /// \brief operator = copy
    /// \param p_playlist
    ///
    inline playlist & operator=(const playlist & p_playlist) {
        if (this != &p_playlist) {
            entity::operator =(p_playlist);
            m_tracks = p_playlist.m_tracks;
        }
        return *this;
    }


    /// not allowed
    void *operator new[] (size_t) = delete;

    /// not allowed
    void operator delete[] (void *) = delete;


    // ---------------------------------------------
    private:

    ///
    /// \brief m_tracks the tracks of the playlist
    ///
    tracks m_tracks;

};


    // ==> Typedefs

    ///
    /// \brief playlists collection of playlists
    ///
    typedef std::set<playlist> playlists;

    // ==> Attributes

    // ==> Functions



}
}
}


#endif
