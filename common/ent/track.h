#ifndef spotify__common__ent__track__H
#define spotify__common__ent__track__H


/// \file In this file is implemented the spotify::common::obj::track class,
/// which represents a track in Spotify

/// \author Rodrigo Canellas

/// \date may/2018

// ==> Headers C++
#include <cstdint>
#include <map>

// ==> Qt Headers
#include <QString>
#include <QUrl>

// ==> 3rd Headers

// ==> Our Headers
#include <spotify/common/ent/entity.h>

// ==> Namespaces
using namespace std;

// ==> Macro Constants

// ==> Macro Commands

// project
namespace spotify {
// artifact
namespace common {
// layer
namespace ent {


// ==> Pre-Declarations

// ==> Attributes

// ==> Functions

// ==> Classes

/// \brief
class track : public spotify::common::ent::entity {

    // ---------------------------------------------

public:


    /// not allowed
    track() = delete;

    ///
    /// \brief track
    /// \param p_id
    /// \param m_url
    /// \param p_name
    /// \param p_type
    ///
    inline track(const id & p_id
                 , const QUrl & p_url
                 , const QString & p_name) :
        entity(p_id, p_url, p_name, type::track) {}

    ///
    /// \brief track
    /// \param p_id
    /// \param p_url
    /// \param p_name
    ///
    inline track(id && p_id
                 , QUrl && p_url
                 , QString && p_name) :
        entity(std::move(p_id)
               , std::move(p_url)
               , std::move(p_name)
               , type::track) {}

    ///
    /// \brief track copy constructor
    /// \param p_track
    ///
    inline track(const track & p_track)
        : entity(p_track) {}

    ///
    /// \brief track move constructor
    /// \param p_track
    ///
    inline track(track && p_track)
        : entity(std::move(p_track)) {}

    /// Destructor
    inline ~track() {}

    /// not allowed
    void *operator new[] (size_t) = delete;

    /// not allowed
    void operator delete[] (void *) = delete;

    // ---------------------------------------------
    private:

};


    // ==> Typedefs

    ///
    /// \brief tracks collection of \p (pointers to) tracks objects
    ///
    typedef std::set<track> tracks;

    // ==> Attributes

    // ==> Functions



}
}
}

#endif
