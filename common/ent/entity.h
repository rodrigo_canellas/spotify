#ifndef spotify__common__ent__entity__H
#define spotify__common__ent__entity__H


/// \file In this file is implemented the spotify::common::ent::entity, which
/// represents the common object for all the Spotify objects, like albuns,
/// artists, tracks, etc.

/// \author Rodrigo Canellas

/// \date may/2018

// ==> Headers C++
#include <cstdint>
#include <set>
#include <utility>
#include <memory>

// ==> Qt Headers
#include <QString>
#include <QUrl>

// ==> 3rd Headers

// ==> Our Headers


// ==> Namespaces
using namespace std;

// ==> Macro Constants

// ==> Macro Commands


// project
namespace spotify {
// artifact
namespace common {
// layer
namespace ent {

// ==> Typedefs

// ==> Pre-Declarations

// ==> Attributes

// ==> Functions

template <typename t_obj>
void merge(std::set<t_obj> & p_to, const std::set<t_obj> & p_from) {
    using namespace std;
    typename set<t_obj>::const_iterator _end = p_from.end();
    typename set<t_obj>::const_iterator _ite = p_from.begin();
    for (;_ite != _end; ++_ite) {
        p_to.emplace(*_ite);
    }
}

// ==> Classes

///
/// \brief The entity class represents the common object for all the Spotify
/// objects, like albuns, artists, tracks, etc.
///
class entity  {

    // ---------------------------------------------

public:

    ///
    /// \brief id type of the identifier of the object, within the Spotify
    ///
    typedef QString id;

    /// \bried type is the type of the object, according to Spotify
    ///  classification
    enum class type : uint8_t { album
                                , artist
                                , track
                                , playlist
                                , image
                                , undefined};


    ///
    /// \brief entity
    ///
    entity()
        : m_id("--undefined--")
        , m_url(QUrl(""))
        , m_name("--undefined--")
        , m_type(type::undefined) {}

    ///
    /// \brief entity copy constructor
    /// \param p_object
    ///
    inline entity(const entity & p_object)
        : m_id(p_object.m_id)
        , m_url(p_object.m_url)
        , m_name(p_object.m_name)
        , m_type(p_object.m_type) {}


    ////
    /// \brief entity move constructor
    /// \param p_object
    ///
    inline entity(entity && p_object)
        : m_id(std::move(p_object.m_id))
        , m_url(std::move(p_object.m_url))
        , m_name(std::move(p_object.m_name))
        , m_type(std::move(p_object.m_type)) {}

    /// Destructor
    inline virtual ~entity() {}

    ///
    /// \brief get_id retrieves unique identifier of an entity within all the
    /// Spotify universe
    /// \return
    ///
    inline const id & get_id() const {
        return m_id;
    }

    ///
    /// \brief get_url retrieves external URL where the entity can be accessed
    /// \return
    ///
    inline const QUrl & get_url() const {
        return m_url;
    }

    ///
    /// \brief get_name retrieves the human name of the entity
    /// \return
    ///
    inline const QString & get_name() const {
        return m_name;
    }

    ///
    /// \brief get_type retrives the type of the entity, according to Spotify
    /// classification
    /// \return
    ///
    inline type get_type() const {
        return m_type;
    }

    ///
    /// \brief type_to_str translates the \p type of the entity into a string,
    /// recognized by Spotify
    /// \param p_type
    /// \return
    ///
    static QString type_to_str(type p_type) {
        switch (p_type) {
        case type::artist:
            return "artist";
        case type::track:
            return "track";
        case type::playlist:
            return "playlist";
        case type::image:
            return "image";
        case type::album:
            return "album";
        case type::undefined:
            return "--undefined--";
        }
        return "unknown";
    }

    ///
    /// \brief operator = copy
    /// \param p_object
    /// \return
    ///
    inline entity & operator=(const entity & p_object) {
        if (this != &p_object) {
            m_id = p_object.m_id;
            m_url = p_object.m_url;
            m_name = p_object.m_name;
            m_type = p_object.m_type;
        }
        return *this;
    }

    ///
    /// \brief operator = move
    /// \param p_object
    /// \return
    ///
    inline entity & operator=(entity && p_object) {
        if (this != &p_object) {
            m_id = std::move(p_object.m_id);
            m_url = std::move(p_object.m_url);
            m_name = std::move(p_object.m_name);
            m_type = std::move(p_object.m_type);
        }
        return *this;
    }

    ///
    /// \brief operator == two objects are equal if theirs ids are equal
    /// \param p_object
    /// \return
    ///
    inline bool operator ==(const entity & p_object) const {
        return (m_id == p_object.m_id);
    }

    ///
    /// \brief operator != two objects are not equal if theirs ids are different
    /// \param p_object
    /// \return
    ///
    inline bool operator !=(const entity & p_object) const {
        return (m_id != p_object.m_id);
    }

    ///
    /// \brief operator <
    /// \param p_object
    /// \return
    ///
    inline bool operator< (const entity & p_object) const {
        return m_id < p_object.m_id;
    }

    /// not allowed
    void *operator new[] (size_t) = delete;

    /// not allowed
    void operator delete[] (void *) = delete;

    // ---------------------------------------------
    protected:

    ///
    /// \brief entity constructor
    /// \param p_id unique identifier of an entity within all the Spotify
    /// universe
    /// \param m_url external URL where the entity can be accessed
    /// \param p_name human name of the entity
    /// \param p_type type of the Spotify entity
    ///
    entity(const id & p_id
    , const QUrl & p_url
    , const QString & p_name
    , type p_type)
    : m_id(p_id)
    , m_url(p_url)
    , m_name(p_name)
    , m_type(p_type) {}

    ///
    /// \brief entity
    /// \param p_id
    /// \param p_url
    /// \param p_name
    /// \param p_type
    ///
    entity(id && p_id
           , QUrl && p_url
           , QString && p_name
           , type p_type)
        : m_id(std::move(p_id))
        , m_url(std::move(p_url))
        , m_name(std::move(p_name))
        , m_type(p_type) {}


    // ---------------------------------------------
private:

    ///
    /// \brief m_id unique identifier of an entity within all the Spotify
    /// universe
    ///
    id m_id;

    ///
    /// \brief m_url external URL where the entity can be accessed
    ///
    QUrl m_url;

    ///
    /// \brief m_name human name of the entity
    ///
    QString m_name;

    ///
    /// \brief m_type type of the Spotify entity
    ///
    type m_type;


};





}
}
}

#endif
