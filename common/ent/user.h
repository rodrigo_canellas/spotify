#ifndef spotify__common__ent__user__H
#define spotify__common__ent__user__H


/// \file In this file is implemented the class spotify::common::obj::user

/// \author Rodrigo Canellas

/// \date may/2018

// ==> Headers C++
#include <cstdint>

// ==> Qt Headers
#include <QString>

// ==> 3rd Headers

// ==> Our Headers


// ==> Namespaces
using namespace std;

// ==> Macro Constants

// ==> Macro Commands

// project
namespace spotify {
// artifact
namespace common {
// layer
namespace ent {

// ==> Typedefs

// ==> Pre-Declarations

// ==> Attributes

// ==> Functions

// ==> Classes

///
/// \brief The user class represents a user of Spotify
///
class user {

    // ---------------------------------------------

public:


    /// not allowed
    inline user()
        : m_name("")
        , m_id("") {}
    ///
    /// \brief user constructor by copying values
    /// \param p_name
    /// \param p_id
    ///
    inline user(const QString & p_name, const QString & p_id)
        : m_name(p_name)
        , m_id(p_id) {}

    ///
    /// \brief user constructor by moving values
    /// \param p_name
    /// \param p_id
    ///
    inline user(QString && p_name, QString && p_id)
        : m_name(std::move(p_name))
        , m_id(std::move(p_id)) {}

    ///
    /// \brief user copy constructor
    /// \param p_user
    ///
    inline user(const user & p_user)
        : m_name(p_user.m_name)
        , m_id(p_user.m_id) {}


    ///
    /// \brief user move constructor
    /// \param p_user
    ///
    inline user(user && p_user)
        : m_name(std::move(p_user.m_name))
        , m_id(std::move(p_user.m_id)) {}

    ///
    /// \brief ~user
    ///
    ~user();

    ///
    /// \brief get_name
    /// \return
    ///
    const QString & get_name() const {
        return m_name;
    }

    ///
    /// \brief get_id
    /// \return
    ///
    const QString & get_id() const {
        return m_id;
    }

    ///
    /// \brief operator = copy
    /// \return
    ///
    inline user & operator=(const user & p_user) {
        if (this != &p_user) {
            m_name = p_user.m_name;
            m_id = p_user.m_id;
        }
        return *this;
    }

    ///
    /// \brief operator = move
    /// \param p_user
    /// \return
    ///
    inline user & operator=(user && p_user) {
        if (this != &p_user) {
            m_name = std::move(p_user.m_name);
            m_id = std::move(p_user.m_id);
        }
        return *this;
    }

    ///
    /// \brief operator ==
    /// \param p_user
    /// \return
    ///
    inline bool operator==(const user & p_user) {
        return ((m_name == p_user.m_name) &&
                (m_id == p_user.m_id));
    }

    ///
    /// \brief operator !=
    /// \param p_user
    /// \return
    ///
    inline bool operator!=(const user & p_user) {
        return ((m_name != p_user.m_name) ||
                (m_id != p_user.m_id));
    }


    /// not allowed
    void *operator new[] (size_t) = delete;

    /// not allowed
    void operator delete[] (void *) = delete;

    // ---------------------------------------------
    private:

    ///
    /// \brief m_name
    ///
    QString m_name;

    ///
    /// \brief m_id
    ///
    QString m_id;

};



}
}
}

#endif
