#ifndef spotify__common__per__db_local__H
#define spotify__common__per__db_local__H


/// \file In this file are implemented the actions that can be executed by
/// Spotify service, as described in
/// https://developer.spotify.com/documentation/web-api/reference/

/// \author Rodrigo Canellas

/// \date may/2018

// ==> Headers C++
#include <cstdint>
#include <utility>

// ==> Qt Headers
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
//#include <QtSql/QSqlRecord>

#include <QObject>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDateTime>
#include <QCoreApplication>


#include <QDebug>


// ==> 3rd Headers

// ==> Our Headers
#include <spotify/common/ent/user.h>
#include <spotify/common/ent/track.h>
#include <spotify/common/ent/playlist.h>
#include <spotify/common/ent/messages.h>

// ==> Namespaces
using namespace std;
using namespace spotify::common;

// ==> Macro Constants

// ==> Macro Commands

// project
namespace spotify {
// artifact
namespace common {
// layer
namespace per {

// ==> Typedefs



// ==> Pre-Declarations

// ==> Attributes

// ==> Functions

// ==> Classes
struct i_sender {
    virtual QJsonDocument request(const QUrl & p_url) = 0;
};


/// \brief db_local retrieves and saves objects
/// \tparam t_sender must implement
/// \code
/// QJsonDocument request(const QUrl & p_url);
/// \endcode
//template <typename t_sender>
class db_local  {

    // ---------------------------------------------

public:

    ///
    /// \brief db_local
    /// \param p_sender
    /// \param p_db_path is the path of the local db file
    ///
    db_local(i_sender * p_sender
             , const QString & p_db_path = "./");


    /// not allowed
    inline db_local(const db_local &) = delete;

    /// not allowed
    inline db_local(db_local &&) = delete;

    /// Destructor
    inline ~db_local() {}

    ///
    /// \brief load_messages loads the messages associated to
    /// spotify::common::ent::msg_id
    ///
    /// \param p_language is a code the identifies the language of the messages,
    /// as defined in http://www.lingoes.net/en/translator/langcode.htm. The
    /// default is Brazilian Portuguese
    ///
    bool load_messages(ent::messages & p_messagess
                       , const ent::language &p_language = "pt-br");

    ///
    /// \brief read_user_public reads public info about the current user
    bool get_user_public(ent::user & p_user);


    ///
    /// \brief insert_user
    /// \param p_user
    /// \return \p true if there were no errors
    /// \attention this is a private method
    bool insert_user(const ent::user & p_user);

    ///
    /// \brief get_user
    /// \param p_user_id
    /// \param p_exists indicates if the user exists
    /// \return \p first is \p true it there were no errors
    ///
    bool get_user(const ent::entity::id & p_user_id
                                        , bool & p_exists
                                        , ent::user & p_user);

    ///
    /// \brief exists_user
    /// \param p_user_id
    /// \param p_exists \p true if the user exists
    /// \return \p true if it there were no errors
    ///
    bool exists_user(const ent::entity::id & p_user_id
                     , bool & p_exists);

    ///
    /// \brief search_track
    /// \param p_query_filter
    /// \return \p true if it there were no errors
    ///
    bool search_track(const QString & p_query_filter, ent::tracks & p_tracks);

    ///
    /// \brief create_playlist creates a playlist
    /// \param p_user_id is the id of user who is creating the playlist
    /// \param p_name the name of the playlist, as choosen by \p user
    /// \param p_playlist is the playlist created, if possible
    /// \return \p true if it there were no errors
    ///
    bool insert_playlist(const ent::entity::id & p_user_id
                         , const QString & p_name
                         , ent::playlist & p_playlist);


    ///
    /// \brief insert_track inserts a track into a playlist
    /// \param p_track_id
    /// \param p_nome
    /// \param p_url
    /// \param p_tracklist
    /// \return \p true if it there were no errors
    ///
    bool insert_track(const ent::track &p_track
    , const ent::entity::id & p_playist);

    ///
    /// \brief remove_track removes a track from the playlist it belongs
    /// \param p_track_id
    /// \param p_playlist_id
    /// \return
    ///
    bool remove_track(const ent::entity::id &p_track_id
                      , const ent::entity::id &p_playlist_id);

    ///
    /// \brief remove_playlist removes a playlist, along with all the its tracks
    /// \param p_playlist_id
    /// \return
    ///
    bool remove_playlist(const ent::entity::id &p_playlist_id);

    /// not allowed
    inline db_local & operator=(const db_local &) = delete;

    /// not allowed
    inline db_local & operator=(db_local &&) = delete;

    /// not allowed
    void *operator new[] (size_t) = delete;

    /// not allowed
    void operator delete[] (void *) = delete;

    ///
    /// \brief exists_playlist
    /// \param p_user_id
    /// \param p_exists
    /// \return \p true if it there were no errors
    ///
    bool exists_playlist(const ent::entity::id &p_user_id, bool &p_exists);

    ///
    /// \brief exists_track
    /// \param p_user_id
    /// \param p_exists
    /// \return \p true if it there were no errors
    ///
    bool exists_track(const ent::entity::id &p_user_id, bool &p_exists);

    ///
    /// \brief load_playlists loads the playlists of a user
    /// \param p_user_id
    /// \param m_playlists
    /// \return \p true if it there were no errors
    ///
    bool load_playlists(const ent::entity::id & p_user_id
    , ent::playlists & m_playlists);

    ///
    /// \brief load_tracks loads all tracks of a playlist
    /// \param p_playlist_id
    /// \param _tracks
    /// \return \p true if it there were no errors
    ///
    bool load_tracks(const ent::entity::id &p_playlist_id
    , ent::tracks &_tracks);

    private:

    ///
    /// \brief exists generic checker for the existance of an entity
    /// \param p_table the table where to look for
    /// \param p_id the id of the entity to be searched
    /// \param p_exists indicates if \p p_id exists in \p p_table
    /// \return \p true if it there were no errors
    ///
    bool exists(const QString &p_table, const ent::entity::id &p_id, bool &p_exists);



    ///
    /// \brief insert_messages inserts the messages in all the known languages
    /// \return \p true if it there were no errors
    ///
    bool insert_messages();

    ///
    /// \brief insert_messages_ptbr
    /// \return \p true if it there were no errors
    ///
    bool insert_messages_ptbr();

    ///
    /// \brief insert_message
    /// \param p_language
    /// \param p_id
    /// \param p_msg
    /// \return
    ///
    void insert_message(const ent::language &p_lng_db_id
    , ent::msg_id p_id
    , const QString & p_msg);

    ///
    /// \brief get_language_db_id
    /// \param p_language
    /// \return \p true if it there were no errors
    ///
    bool get_language_db_id(ent::language & p_id
    , const QString & p_language = "pt-br");

    ///
    /// \brief generate_id generates a "random" identifier
    /// \return
    ///
    ent::entity::id generate_id() const;

    ///
    /// \brief init opens, or creates, the SQLite file
    /// \return
    ///
    bool init();

    ///
    /// \brief m_sender
    ///
    i_sender * m_sender;

    ///
    /// \brief m_file is the file that contains the peristed data
    ///
    QString m_file;

    ///
    /// \brief m_sqlite
    ///
    QSqlDatabase m_sqlite;

};


}
}
}


#endif
