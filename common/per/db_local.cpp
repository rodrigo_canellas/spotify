#include <spotify/common/per/db_local.h>

#include <thread>
#include <chrono>

using namespace spotify::common;

// -----------------------------------------------------------------------------
ent::entity::id per::db_local::generate_id() const {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    return ent::entity::id(QString::number(QDateTime::currentMSecsSinceEpoch()));
}


// -----------------------------------------------------------------------------
per::db_local::db_local(per::i_sender * p_sender
                        , const QString & p_db_path)
    : m_sender(p_sender)
    , m_file(p_db_path + "/spotify.db")
    , m_sqlite(QSqlDatabase::addDatabase("QSQLITE")) {

    if (!QSqlDatabase::drivers().contains("QSQLITE")) {
        throw std::runtime_error("Driver do SQLite não encontrado");
    }

    if (!init()) {
        throw std::runtime_error("Não foi possível abrir/criar base SQLite "
                                 "no arquivo " + m_file.toStdString());
    }
}

// -----------------------------------------------------------------------------
bool per::db_local::search_track(const QString & p_query_filter
                                 , ent::tracks & p_tracks) {

    QString _str("https://api.spotify.com/v1/search");

    qDebug() << "search_track_action::operator() started";

    QString _what = "&type=track";

    _str += "?q=" + p_query_filter + _what;

    qDebug() << "str = " << _str;

    QUrl _url (_str);

    qDebug() << "search_track_action::operator(), url = " << _url;

    QJsonDocument _doc(m_sender->request(_url));

    const QJsonObject _root (_doc.object());

    //qDebug() << "search_track_action::operator(), json = " << _root;

    QJsonObject _tracks_json = _root["tracks"].toObject();

    QJsonArray _items = _tracks_json["items"].toArray();

    for (const QJsonValue & _value : _items) {
        QJsonObject _item = _value.toObject();
        ent::entity::id _id = _item["id"].toString();
        QString _name = _item["name"].toString();
        QUrl _url = QUrl(_item["preview_url"].toString());
        qDebug() << "search_track_action: id = '" << _id << "'"
                 << ", _name = '" << _name << "'"
                 << ", _url = '" << _url << "'";

        ent::track _track(std::move(_id), std::move(_url), std::move(_name));
        p_tracks.emplace(std::move(_track));
    }

    return true;
}

// -----------------------------------------------------------------------------
bool per::db_local::init() {

    m_sqlite.setDatabaseName(m_file);

    if (!m_sqlite.open()) {
        return false;
    }

    QStringList _tables = m_sqlite.tables();

    QSqlQuery _query;

    // users
    if (!_tables.contains("users", Qt::CaseInsensitive)) {
        if (!_query.exec(QLatin1String("CREATE TABLE `users` ( "
                                       "`id`	TEXT NOT NULL PRIMARY KEY UNIQUE, "
                                       "`name`	TEXT NOT NULL "
                                       ")"))) {
            return false;
        }
    }

    // playlist
    if (!(_tables.contains("playlists", Qt::CaseInsensitive))) {
        if (!_query.exec(QLatin1String("CREATE TABLE `playlists` ("
                                       "`id`	TEXT NOT NULL PRIMARY KEY UNIQUE, "
                                       "`name`	TEXT NOT NULL, "
                                       "`url`	TEXT NOT NULL, "
                                       "`user`	TEXT NOT NULL, "
                                       "FOREIGN KEY(`user`) REFERENCES 'users.id' "
                                       ")"))) {
            return false;
        }
    }

    // tracks
    if (!_tables.contains("tracks", Qt::CaseInsensitive)) {
        if (!_query.exec(QLatin1String("CREATE TABLE `tracks` ("
                                       "`id`	TEXT NOT NULL PRIMARY KEY UNIQUE, "
                                       "`name`	TEXT NOT NULL, "
                                       "`url`	TEXT NOT NULL, "
                                       "`playlist`	TEXT NOT NULL, "
                                       "FOREIGN KEY(`playlist`) REFERENCES 'playlist.id' "
                                       ")"))) {
            return false;
        }
    }

    // languages
    if (!_tables.contains("languages", Qt::CaseInsensitive)) {
        if (!_query.exec(QLatin1String("CREATE TABLE `languages` ("
                                       "`id`	TEXT NOT NULL PRIMARY KEY UNIQUE "
                                       ")"))) {
            return false;
        }
    }

    // messages
    if (!(_tables.contains("messages", Qt::CaseInsensitive))) {
        if (!_query.exec(QLatin1String("CREATE TABLE `messages` ("
                                       "`id`	INTEGER NOT NULL PRIMARY KEY UNIQUE, "
                                       "`msg`	TEXT NOT NULL, "
                                       "`language`	TEXT NOT NULL,"
                                       "FOREIGN KEY(`language`) REFERENCES 'languages.id' "
                                       ")"))) {
            return false;
        }
    }

    return insert_messages();
}

// -----------------------------------------------------------------------------
bool per::db_local::insert_playlist(const ent::entity::id & p_user_id
                                    , const QString & p_name
                                    , ent::playlist & p_playlist) {

    bool _user_exists = false;
    if (!exists_user(p_user_id, _user_exists)) {
        return false;
    }

    if (!_user_exists) {
        return false;
    }

    ent::entity::id _playlist_id = generate_id();

    QUrl _url ("http://"+_playlist_id+".com");

    QSqlQuery _query;
    QString _sql("INSERT INTO playlists (id, name, url, user) VALUES ("
                 "'" + _playlist_id + "', " +
                 "'" + p_name + "', " +
                 "'" + _url.toString() + "', " +
                 "'" + p_user_id + "'" +
                 ")");
    _query.prepare(_sql);

    qDebug() << "db_local::create_playlist, sql = '" << _sql << "'";

    if(_query.exec()) {
        p_playlist = std::move(ent::playlist(_playlist_id, _url, p_name));
        return true;
    }

    return false;
}

// -----------------------------------------------------------------------------
bool per::db_local::get_user_public(ent::user & p_user) {
    QUrl _url("https://api.spotify.com/v1/me");

    qDebug() << "user_public_read::operator() started";

    QJsonDocument _doc = m_sender->request(_url);

    const QJsonObject _root (_doc.object());

    QString _id = _root.value("id").toString();
    QString _name = _root.value("display_name").toString();

    qDebug() << "user_public_read::operator(), id = " << _id
             << ", name = " << _name;

    ent::user _user(std::move(_name), std::move(_id));

    // is it already in local db?
    bool _user_exists = false;
    bool _rc_db = exists_user(_user.get_id(), _user_exists);
    if (!_rc_db) {
        return false;
    }

    if (!_user_exists) {
        _rc_db = insert_user(_user);
        if (!_rc_db) {
            return false;
        }
    }

    p_user = _user;
    return true;
}

//// -----------------------------------------------------------------------------
//bool per::db_local::user_exists(const ent::user & p_user, bool & p_exists) {

//    p_exists = false;
//    QSqlQuery _query;
//    _query.prepare("SELECT count(*) FROM users WHERE id = (:id)");
//    _query.bindValue(":id", p_user.get_id());

//    if (!_query.exec()) {
//        return false;
//    }

//    if (_query.next()) {
//        int _val = _query.value(0).toInt();
//        if (_val != 0) {
//            p_exists = true;
//        }
//    }

//    return true;

//}

// -----------------------------------------------------------------------------
bool per::db_local::insert_track(const ent::track & p_track
                                 , const ent::entity::id &p_playist) {

    bool _track_exists = false;
    if (!exists_track(p_track.get_id(), _track_exists)) {
        return false;
    }

    if (_track_exists) {
        // no need to insert if it exists
        return true;
    }

    bool _playlist_exists = false;
    if (!exists_playlist(p_playist, _playlist_exists)) {
        return false;
    }

    if (!_playlist_exists) {
        qDebug() << "db_local::insert_track, playlist '" << p_playist
                 << "' does not exist";
        return false;
    }

    QSqlQuery _query;
    QString _sql("INSERT INTO tracks (id, name, url, playlist) VALUES ("
                 "'" + p_track.get_id() + "', " +
                 "'" + p_track.get_name() + "', " +
                 "'" + p_track.get_url().toString() + "', " +
                 "'" + p_playist + "'" +
                 ")");
    _query.prepare(_sql);

    qDebug() << "db_local::insert_track, sql = '" << _sql << "'";

    if(_query.exec()) {
        return true;
    }

    return false;
}

// -----------------------------------------------------------------------------
bool per::db_local::remove_track(const ent::entity::id &p_track_id
                                 , const ent::entity::id &p_playlist_id) {
    QString _sql = "DELETE FROM tracks WHERE id = '" +
            p_track_id + "' " +
            "AND playlist = '" + p_playlist_id + "'";

    qDebug() << "db_local::remove_track, sql = " << _sql;

    QSqlQuery _query;
    _query.prepare(_sql);

    if (!_query.exec()) {
        return false;
    }

    return true;
}

bool per::db_local::remove_playlist(const ent::entity::id &p_playlist_id) {

    m_sqlite.transaction();
    {
        QString _sql_tracks = "DELETE FROM tracks WHERE playlist = '"
                + p_playlist_id + "'";

        qDebug() << "db_local::remove_playlist, sql_tracks = " << _sql_tracks;

        QSqlQuery _query_tracks;
        _query_tracks.prepare(_sql_tracks);

        if (!_query_tracks.exec()) {
            m_sqlite.rollback();
            return false;
        }
    }

    {
        QString _sql_playlist = "DELETE FROM playlists WHERE id = '"
                + p_playlist_id + "'";

        qDebug() << "db_local::remove_playlist, sql_playlist = "
                 << _sql_playlist;

        QSqlQuery _query_playlist;
        _query_playlist.prepare(_sql_playlist);

        if (!_query_playlist.exec()) {
            m_sqlite.rollback();
            return false;
        }
    }

    m_sqlite.commit();
    return true;


}



//// -----------------------------------------------------------------------------
//bool per::db_local::playlist_exists(const ent::entity::id & p_playlist_id
//                                    , bool & p_exists) {
//    p_exists = false;
//    QSqlQuery _query;
//    _query.prepare("SELECT count(*) FROM playlists WHERE id = (:id)");
//    _query.bindValue(":id", p_playlist_id);

//    if (!_query.exec()) {
//        return false;
//    }

//    if (_query.next()) {
//        p_exists = true;
//        return true;
//    }
//    return false;
//}

// -----------------------------------------------------------------------------
bool per::db_local::insert_user(const ent::user & p_user) {

    bool _user_exists = false;
    if (!exists_user(p_user.get_id(), _user_exists)) {
        return false;
    }

    if (_user_exists) {
        // no need to insert if it exists
        return true;
    }

    QSqlQuery _query;
    QString _sql("INSERT INTO users (id, name) VALUES ("
                 "'" + p_user.get_id() + "', "
                                         "'" + p_user.get_name() + "')");
    _query.prepare(_sql);

    qDebug() << "sql = '" << _sql << "'";

    if(_query.exec()) {
        return true;
    }

    return false;

}

// -----------------------------------------------------------------------------
bool per::db_local::get_user(const ent::entity::id &p_user_id
                             , bool &p_exists
                             , ent::user &p_user) {

    p_exists = false;
    QSqlQuery _query;
    _query.prepare("SELECT name, id FROM users WHERE id = (:id)");
    _query.bindValue(":id", p_user_id);

    if (!_query.exec()) {
        return false;
    }

    if (_query.next()) {
        p_user = ent::user(_query.value(0).toString()
                           , _query.value(1).toString());
        p_exists = true;
    }
    return true;
}

bool per::db_local::exists(const QString & p_table
                           , const ent::entity::id & p_id
                           , bool & p_exists) {
    QString _sql ("SELECT count(*) FROM " + p_table +
                  " WHERE id = '" + p_id + "'");


    p_exists = false;
    QSqlQuery _query;
    _query.prepare(_sql);

    qDebug() << "db_local::exists, sql = '" << _sql << "'";

    if ( (!_query.exec()) || (!_query.next()) ) {
        return false;
    }

    if (_query.value(0).toInt() != 0) {
        p_exists = true;
    }
    return true;

}


// -----------------------------------------------------------------------------
bool per::db_local::exists_user(const ent::entity::id &p_user_id
                                , bool &p_exists) {

    return exists("users", p_user_id, p_exists);
}


// -----------------------------------------------------------------------------
bool per::db_local::exists_playlist(const ent::entity::id &p_user_id
                                    , bool &p_exists) {

    return exists("playlists", p_user_id, p_exists);
}

// -----------------------------------------------------------------------------
bool per::db_local::exists_track(const ent::entity::id &p_user_id
                                 , bool &p_exists) {

    return exists("tracks", p_user_id, p_exists);
}

// -----------------------------------------------------------------------------
bool per::db_local::load_playlists(const ent::entity::id & p_user_id
                                   , ent::playlists &p_playlists) {
    // user exists?
    bool _user_exists = false;
    if (!exists_user(p_user_id, _user_exists)) {
        return false;
    }

    if (!_user_exists) {
        return false;
    }

    QString _sql("SELECT id, name, url FROM playlists WHERE user = '"
                 + p_user_id + "'");

    qDebug() << "db_local::load_playlists, sql = " << _sql;

    QSqlQuery _query;
    _query.prepare(_sql);

    if (!_query.exec()) {
        return false;
    }

    while (_query.next()) {
        ent::entity::id _playlist_id(_query.value(0).toString());
        QString _name(_query.value(1).toString());
        QUrl _url (_query.value(2).toString());
        qDebug() << "db_local::load_playlists: "
                 << "playlist_id = '" << _playlist_id << "', "
                 << "name = '" << _name << "', "
                 << "url = '" << _url << "'";

        ent::tracks _tracks;
        if (!load_tracks(_playlist_id, _tracks)) {
            return false;
        }

        ent::playlist _playlist(std::move(_playlist_id)
                                , std::move(_url)
                                , std::move(_name));
        _playlist.set(std::move(_tracks));
        p_playlists.insert(std::move(_playlist));
    }
    return true;
}

///
/// \brief per::db_local::load_tracks
/// \param p_playlist_id
/// \param _tracks
/// \return
///
bool per::db_local::load_tracks(const ent::entity::id &p_playlist_id
                                , ent::tracks &p_tracks) {

    QString _sql("SELECT id, name, url from TRACKS where playlist = '" +
                 p_playlist_id + "'");

    QSqlQuery _query;
    _query.prepare(_sql);
    if (!_query.exec()) {
        return false;
    }
    while (_query.next()) {
        ent::entity::id _track_id (_query.value(0).toString());
        QString _name (_query.value(1).toString());
        QUrl _url(_query.value(2).toString());
        qDebug() << "db_local::load_tracks: "
                 << "track id = '" << _track_id << "', "
                 << "name = '" << _name << ", "
                 << "url = '" << _url << "'";
        ent::track _track(std::move(_track_id)
                          , std::move(_url)
                          , std::move(_name));
        p_tracks.insert(std::move(_track));
    }

    return true;

}

// -----------------------------------------------------------------------------
bool per::db_local::load_messages(ent::messages & p_messages
                                  , const ent::language& p_language) {

    qDebug() << "db_local::load_messages starting";

    ent::messages _messages;

    QString _lng = p_language.toLower();

    QString _lng_db_id;
    if (!get_language_db_id(_lng_db_id, _lng)) {
        return false;
    }

    QString _sql("SELECT id, msg FROM messages WHERE language = '" +
                 _lng_db_id + "'");

    qDebug() <<  "db_local::load_messages, sql = " << _sql;
    QSqlQuery _query(_sql);
    while (_query.next())
    {
        ent::msg_id _msg_id = _query.value(0).toInt();
        QString _msg = _query.value(1).toString();
        qDebug() << "msg_id = " << _msg_id << ", "
                 << "msg = " << _msg;
        p_messages.insert(make_pair(move(_msg_id), move(_msg)));
    }

    return true;

}

// -----------------------------------------------------------------------------
bool per::db_local::insert_messages() {

    return insert_messages_ptbr();
}

// -----------------------------------------------------------------------------
bool per::db_local::insert_messages_ptbr() {

    using namespace spotify::common::ent;
    using namespace std;

    QString _lng_id;

    if (!get_language_db_id( _lng_id, "pt-br")) {
        return false;
    }

    insert_message(_lng_id
                   , error_loading_user_public
                   , "Error lendo dados públicos do usuário");

    insert_message(_lng_id
                   , error_authorizing_user
                   , "Erro autorizando usuário");

    insert_message(_lng_id
                   , error_searching_for_tracks
                   , "Erro recuperando trilhas");

    insert_message(_lng_id
                   , error_loading_playlists
                   , "Erro recuperando trilhas");

    insert_message(_lng_id
                   , error_creating_playlist
                   , "Erro inserindo nova playlist");

    insert_message(_lng_id
                   , error_inserting_track_into_playlist
                   , "Erro inserindo música na playlist");

    insert_message(_lng_id
                   , error_removing_track_from_playlist
                   , "Erro removendo música na playlist");

    insert_message(_lng_id
                   , error_removing_playlist
                   , "Erro removendo playlist");

    return true;

}

// -----------------------------------------------------------------------------
void per::db_local::insert_message(const ent::language &p_lng_db_id
                                   , ent::msg_id p_id
                                   , const QString & p_msg) {

    QSqlQuery _query;
    QString _sql("INSERT INTO messages (id, msg, language) VALUES (" +
                 QString::number(p_id) + ", "
                                         "'" + p_msg + "', " +
                 "'" + p_lng_db_id + "'" +
                 ")");
    _query.prepare(_sql);
    _query.exec();

    qDebug() << "db_local::insert_message, sql = '" << _sql << "'";
}

// -----------------------------------------------------------------------------
bool per::db_local::get_language_db_id(ent::language & p_lng_id
                                       , const QString & p_language) {
    QString _sql_select = "SELECT id FROM languages where id = '"
            + p_language + "'";
    qDebug() << "db_local::get_language_db_id, sql select = '"
             << _sql_select << "'";

    QSqlQuery _query(_sql_select);
    if (!_query.exec()) {
        qDebug() << "db_local::get_language_db_id, erro no QSqlQuery::exec "
                    "do select";
        return false;
    }
    if (!_query.next()) {

        QString _sql_insert("INSERT INTO languages (id) VALUES "
                     "('" + p_language + "')" );
        qDebug() << "db_local::get_language_db_id, sql insert = '"
                 << _sql_insert << "'";
        QSqlQuery _ins;
        _ins.prepare(_sql_insert);
        if (!_ins.exec()) {
            qDebug() << "db_local::get_language_db_id, falha QSqlQuery::exec "
                        "do no sql insert";
            return false;
        }

        return get_language_db_id(p_lng_id, p_language);
    }
    p_lng_id = _query.value(0).toString();
    qDebug() << "db_local::get_language_db_id, lng = " << p_lng_id;
    return true;
}
